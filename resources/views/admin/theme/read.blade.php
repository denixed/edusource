@extends('layouts.admin',[
    'page_header'=>'Просмотр экзамена'
])

@section('content')

    <div>
        <a href="{{route('admin.theme.edit',[
                                'theme'=>$theme->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить этот экзамен?');" href="{{route('admin.theme.delete',[
                                'theme'=>$theme->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр темы </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$theme->id}}</td>
                    </tr>
                    <tr>
                        <th>Номер</th>
                        <td>{{$theme->number}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$theme->name}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$theme->content}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
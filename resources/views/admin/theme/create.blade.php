@extends('layouts.admin',[
    'page_header'=>'Создать новую тему'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новую тему</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/theme/form',[
                'action'=>route('admin.theme.store',[
                    'subject'=>$subject->id
                ]),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
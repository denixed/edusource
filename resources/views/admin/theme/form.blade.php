<form method="POST" action="{{$action}}">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Название темы</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$formData->name ?? '')  }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="number" class="col-md-4 col-form-label text-md-right">Номер задания</label>
        <div class="col-md-6">
            <input id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number',$formData->number ?? '')  }}" required autofocus>
            @if ($errors->has('number'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('number') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="content" class="col-md-4 col-form-label text-md-right">Описание темы</label>
        <div class="col-md-12">
            <textarea id="content" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" required>{{ old('content',$formData->content ?? '') }}</textarea>
            @if ($errors->has('content'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="in_exam" class="col-md-4 col-form-label text-md-right">Статус</label>
        <div class="col-md-6">
            <select id="in_exam" type="text" class="form-control{{ $errors->has('in_exam') ? ' is-invalid' : '' }}" name="in_exam" required>
                <option value="0" {{(old('in_exam',$formData->in_exam??0))==0?'selected':''}}> Не входит в экзамен </option>
                <option value="1" {{(old('in_exam',$formData->in_exam??0))==1?'selected':''}}> Входит в экзамен данного года </option>
            </select>
            @if ($errors->has('in_exam'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('in_exam') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
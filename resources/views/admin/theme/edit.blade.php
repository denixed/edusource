@extends('layouts.admin',[
    'page_header'=>'Редактировать тему'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать тему </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/theme/form',[
                'action'=>route('admin.theme.update',[
                    'theme' => $theme->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $theme
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
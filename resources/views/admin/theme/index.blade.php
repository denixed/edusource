@extends('layouts.admin',[
    'page_header'=>'Темы'
])

@section('content')
    <div>
        <a href="{{route('admin.theme.create',[
            'subject'=>$subject->id
        ])}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить новую тему</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="data-table table table-bordered table-striped">
                <col width="auto" span="4">
                <col width="150">
                <thead>
                <tr>
                    <th>#</th>
                    <th>id</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($themes as $theme)
                    <tr>
                        <th>{{$theme->number}}</th>
                        <th>#{{$theme->id}}</th>
                        <td>
                            {{$theme->name}}
                            <ul class="list-group">
                                @foreach($theme->prots as $prot)
                                    <a href="{{route('admin.prot.read',[
                                        'prot'=>$prot
                                    ])}}" class="list-group-item  list-group-item-action">{{$prot->name}}</a>
                                @endforeach
                                <a href="{{route('admin.prot.create',[
                                    'theme'=>$theme
                                ])}}" class="list-group-item list-group-item-action list-group-item-success">Добавить прототип</a>

                            </ul>

                        </td>
                        <td>{{$theme->content}}</td>
                        <td>
                            <a href="{{route('admin.theme.read',[
                                'theme'=>$theme->id
                            ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                            <a href="{{route('admin.theme.edit',[
                                'theme'=>$theme->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <a onclick="return confirm('Вы действительно хотите удалить тему #{{$theme->name}}?');" href="{{route('admin.theme.delete',[
                                'theme'=>$theme->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
            $('.data-table').DataTable({
                'paging'      : false,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>
@endsection
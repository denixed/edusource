@extends('layouts.admin',[
    'page_header'=>'Создать новый экзамен'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новый экзамен </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/exam/form',[
                'action'=>route('admin.exam.store'),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
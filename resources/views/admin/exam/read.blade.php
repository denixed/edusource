@extends('layouts.admin',[
    'page_header'=>'Просмотр экзамена'
])

@section('content')

    <div>
        <a href="{{route('admin.subject.list',[
                                'exam'=>$exam->id
                            ])}}" class="btn btn-primary"><i class="fa fa-file-text-o"></i> &nbsp;Предметы </a>
        <a href="{{route('admin.exam.edit',[
                                'exam'=>$exam->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить этот экзамен?');" href="{{route('admin.exam.delete',[
                                'exam'=>$exam->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр экзамена {{$exam->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$exam->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$exam->name}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$exam->content}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
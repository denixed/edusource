@extends('layouts.admin',[
    'page_header'=>'Редактировать экзамен'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать экзамен </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/exam/form',[
                'action'=>route('admin.exam.update',[
                    'exam' => $exam->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $exam
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
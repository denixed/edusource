@extends('layouts.admin',[
    'page_header'=>'Редактировать уроки'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.lessons.block.list')}}"> Уроков</a></li>
        <li><a href="{{route('admin.lessons.block.read',[
            'lessonsBlock'=>$lessonsBlock->id
        ])}}">{{$lessonsBlock->title}}</a></li>
        <li class="active">Редактировать</li>
    </ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать уроки </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/lessonsBlock/form',[
                'action'=>route('admin.lessons.block.update',[
                    'page' => $lessonsBlock->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $lessonsBlock
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
@endsection
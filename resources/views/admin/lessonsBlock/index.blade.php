
@extends('layouts.admin',[
    'page_header'=>'Уроки'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li class="active">Уроки</li>
    </ol>
@endsection
@section('content')
    <div>
        <a href="{{route('admin.lessons.block.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить уроки</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->


        <div class="box-body">
            <form action="" class="form-inline">
                <div class="input-group">
                    <input type="search" class="form-control" placeholder="Поиск" name="s" value="{{\Request::get('s')}}">
                </div>
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search"></i>
                </button>

            </form>
            <div class="table-responsive">
                <table class="data-table table table-bordered table-striped">
                    <col width="auto" span="4">
                    <col width="150">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>Название</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lessonsBlocks as $lessonsBlock)
                        <tr>
                            <th>{{$lessonsBlock->id}}</th>
                            <td>
                                @if($lessonsBlock->image_preview)
                                    @image([
                                    'media' => $lessonsBlock->image_preview,
                                    'attrs' => 'style="max-width:50px;"'])

                                @else
                                    -
                                @endif
                            </td>
                            <td>{{$lessonsBlock->title}}</td>
                            <td>
                                <a href="{{route('admin.lessons.block.read',[
                                    'lessonsBlock'=>$lessonsBlock->id
                                ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                <a href="{{route('admin.lesson.list',[
                                    'lessonsBlock'=>$lessonsBlock->id
                                ])}}" class="btn btn-success"><i class="fa fa-file-text-o"></i></a>
                                <a href="{{route('admin.lessons.block.edit',[
                                    'lessonsBlock'=>$lessonsBlock->id
                                ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a onclick="return confirm('Вы действительно хотите удалить уроки #{{$lessonsBlock->id}}?');" href="{{route('admin.lessons.block.delete',[
                                    'lessonsBlock'=>$lessonsBlock->id
                                ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        {{$lessonsBlocks->appends(['s' => \Request::get('s')])->links()}}
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
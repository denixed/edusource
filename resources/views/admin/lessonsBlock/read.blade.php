@extends('layouts.admin',[
    'page_header'=>'Просмотр Уроков'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.lessons.block.list')}}"> Уроков</a></li>
        <li class="active">{{$lessonsBlock->title}}</li>
    </ol>
@endsection

@section('content')

    <div>
        <a href="{{route('admin.lessons.block.edit',[
                                'lessonsBlock'=>$lessonsBlock->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a href="{{route('admin.lesson.list',[
                                    'lessonsBlock'=>$lessonsBlock->id
                                ])}}" class="btn btn-success"><i class="fa fa-file-text-o"></i> Открыть курс</a>
        <a onclick="return confirm('Вы действительно хотите удалить эти уроки?');" href="{{route('admin.lessons.block.delete',[
                                'lessonsBlock'=>$lessonsBlock->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр Уроков: {{$lessonsBlock->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$lessonsBlock->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$lessonsBlock->title}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$lessonsBlock->description}}</td>
                    </tr>

                    <tr>
                        <th>Фоновое изображение</th>
                        <td>
                            @if($lessonsBlock->image_cover)
                                @image([
                                    'media' => $lessonsBlock->image_cover,
                                    'attrs' => 'style="max-width:300px;"'])

                            @else
                                Не загружено
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Превью</th>
                        <td>
                            @if($lessonsBlock->image_preview)
                                @image([
                                    'media' => $lessonsBlock->image_preview,
                                    'attrs' => 'style="max-width:300px;"'])

                            @else
                                Не загружено
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
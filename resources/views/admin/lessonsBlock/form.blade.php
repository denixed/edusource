<form method="POST" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="title" class="col-md-4 col-form-label text-md-right">Заголовок серии уроков</label>
        <div class="col-md-6">
            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title',$formData->title ?? '')  }}" required autofocus>
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">Описание серии</label>
        <div class="col-md-6">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ old('description',$formData->description ?? '') }}</textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="subject" class="col-md-4 col-form-label text-md-right">Предмет / Направление </label>
        <div class="col-md-6">
            <input id="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ old('subject',$formData->subject ?? '')  }}" required autofocus>
            @if ($errors->has('subject'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="subject" class="col-md-4 col-form-label text-md-right">Автор </label>
        <div class="col-md-6">
            <input id="subject" type="text" class="form-control{{ $errors->has('authors.0.name') ? ' is-invalid' : '' }}" name="authors[0][name]" value="{{ old('authors',$formData->authors[0]->name ?? '')  }}" required autofocus>
            @if ($errors->has('authors.0.name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('authors.0.name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="exam" class="col-md-4 col-form-label text-md-right">Экзамен (не обязательно) </label>
        <div class="col-md-6">
            <input id="exam" type="text" class="form-control{{ $errors->has('exam') ? ' is-invalid' : '' }}" name="exam" value="{{ old('exam',$formData->exam ?? '')  }}" autofocus>
            @if ($errors->has('exam'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('exam') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Обложка</label>
        <div class="col-md-6">
            @component('admin.media.upload_form',[
                'formName'=>'image_cover',
                'media'=> old('image_cover',$formData->image_cover ?? null)
            ])
            @endcomponent
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Превью</label>
        <div class="col-md-6">
            @component('admin.media.upload_form',[
                'formName'=>'image_preview',
                'media'=> old('image_preview',$formData->image_preview ?? null)
            ])
            @endcomponent
        </div>
    </div>
    <strong>{{ $errors->first() }}</strong>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
<script src='/adminLTE/js/tinymce/tinymce.min.js'></script>
<script>
    var dfreeBodyConfig = {
        selector: '#context',
        menubar: false,
        height:300,
        plugins: [
            "advlist anchor autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link media noneditable preview",
            " searchreplace table template textcolor visualblocks wordcount"
        ]
    };

    tinymce.init(dfreeBodyConfig);
</script>
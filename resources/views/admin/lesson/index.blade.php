
@extends('layouts.admin',[
    'page_header'=>'Урока'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.lessons.block.list')}}"> Все уроки</a></li>
        <li><a href="{{route('admin.lessons.block.read',[
            'lessonsBlock'=>$lessonsBlock
        ])}}"> {{$lessonsBlock->title}} </a></li>
        <li class="active">Уроки</li>
    </ol>
@endsection
@section('content')
    <div>
        <a href="{{route('admin.lesson.create',[
            'lessonsBlock'=>$lessonsBlock
        ])}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить урок</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->


        <div class="box-body">
            <form action="" class="form-inline">
                <div class="input-group">
                    <input type="search" class="form-control" placeholder="Поиск" name="s" value="{{\Request::get('s')}}">
                </div>
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search"></i>
                </button>

            </form>
            <div class="table-responsive">
                <table class="data-table table table-bordered table-striped">
                    <col width="auto" span="4">
                    <col width="150">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>Название</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lessons as $lesson)
                        <tr>
                            <th>{{$lesson->id}}</th>
                            <td>
                                @if($lesson->image_preview)
                                    @image([
                                    'media' => $lesson->image_preview,
                                    'attrs' => 'style="max-width:50px;"'])

                                @else
                                    -
                                @endif
                            </td>
                            <td>{{$lesson->title}}</td>
                            <td>
                                <a href="{{route('admin.lesson.read',[
                                    'lesson'=>$lesson->id
                                ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                <a href="{{route('admin.lesson.edit',[
                                    'lesson'=>$lesson->id
                                ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a onclick="return confirm('Вы действительно хотите удалить урок #{{$lesson->id}}?');" href="{{route('admin.lesson.delete',[
                                    'lesson'=>$lesson->id
                                ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        {{$lessons->appends(['s' => \Request::get('s')])->links()}}
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
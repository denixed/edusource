<form method="POST" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="title" class="col-md-4 col-form-label text-md-right">Заголовок Урока</label>
        <div class="col-md-6">
            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title',$formData->title ?? '')  }}" required autofocus>
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="video" class="col-md-4 col-form-label text-md-right">Видео урок</label>
        <div class="col-md-6">
            <input id="video" type="text" class="form-control{{ $errors->has('video') ? ' is-invalid' : '' }}" name="video" value="{{ old('video',$formData->video ?? '')  }}" required autofocus>
            @if ($errors->has('video'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('video') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="author" class="col-md-4 col-form-label text-md-right">Автор </label>
        <div class="col-md-6">
            <input id="author" type="text" class="form-control{{ $errors->has('author') ? ' is-invalid' : '' }}" name="author" value="{{ old('author',$formData->author ?? '')  }}" required autofocus>
            @if ($errors->has('author'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('author') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Тип</label>
        <div class="col-md-6">
            <select id="type" type="text" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" required>
                <option value="разбор" {{(old('type')??$formData->type??'')=='разбор'?'selected':''}}> Разбор </option>
                <option value="лекция" {{(old('type')??$formData->type??'')=='лекция'?'selected':''}}> Лекция </option>
                <option value="прочее" {{(old('type')??$formData->type??'')=='прочее'?'selected':''}}> Прочее </option>
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="context" class="col-md-4 col-form-label text-md-right">Содержимое</label>
        <div class="col-md-12">
            <textarea id="context" class="form-control{{ $errors->has('context') ? ' is-invalid' : '' }}" name="context">{{ old('context',$formData->context ?? '') }}</textarea>
            @if ($errors->has('context'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('context') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Превью</label>
        <div class="col-md-6">
            @component('admin.media.upload_form',[
                'formName'=>'image_preview',
                'media'=> old('image_preview',$formData->image_preview ?? null)
            ])
            @endcomponent
        </div>
    </div>
    <strong>{{ $errors->first() }}</strong>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
<script src='/adminLTE/js/tinymce/tinymce.min.js'></script>
<script>
    var dfreeBodyConfig = {
        selector: '#context',
        menubar: false,
        height:300,
        plugins: [
            "advlist anchor autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link media noneditable preview",
            " searchreplace table template textcolor visualblocks wordcount"
        ]
    };

    tinymce.init(dfreeBodyConfig);
</script>
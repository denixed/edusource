@extends('layouts.admin',[
    'page_header'=>'Создать новую урок'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.lessons.block.list')}}"> Все уроки</a></li>
        <li><a href="{{route('admin.lessons.block.read',[
            'lessonsBlock'=>$lessonsBlock
        ])}}"> {{$lessonsBlock->title}} </a></li>
        <li><a href="{{route('admin.lesson.list',[
            'lessonsBlock'=>$lessonsBlock
        ])}}"> Уроки </a></li>
        <li class="active">Создать</li>
    </ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новый урок </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/lesson/form',[
                'action'=>route('admin.lesson.store',[
                    'lessonsBlock' => $lessonsBlock
                ]),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
@endsection
@extends('layouts.admin',[
    'page_header'=>'Редактировать урок'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.lessons.block.list')}}"> Все уроки</a></li>
        <li><a href="{{route('admin.lessons.block.read',[
            'lessonsBlock'=>$lesson->lessonsBlock->id
        ])}}"> {{$lesson->lessonsBlock->title}} </a></li>
        <li><a href="{{route('admin.lesson.list',[
            'lessonsBlock'=>$lesson->lessonsBlock->id
        ])}}"> Уроки </a></li>
        <li><a href="{{route('admin.lesson.read',[
            'lesson'=>$lesson->id
        ])}}">{{$lesson->title}}</a></li>
        <li class="active">Редактировать</li>
    </ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать урок </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/lesson/form',[
                'action'=>route('admin.lesson.update',[
                    'page' => $lesson->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $lesson
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
@endsection
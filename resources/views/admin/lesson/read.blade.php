@extends('layouts.admin',[
    'page_header'=>'Просмотр Урока'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.lessons.block.list')}}"> Все уроки</a></li>
        <li><a href="{{route('admin.lessons.block.read',[
            'lessonsBlock'=>$lesson->lessonsBlock->id
        ])}}"> {{$lesson->lessonsBlock->title}} </a></li>
        <li><a href="{{route('admin.lesson.list',[
            'lessonsBlock'=>$lesson->lessonsBlock->id
        ])}}"> Уроки </a></li>
        <li class="active">{{$lesson->title}}</li>
    </ol>
@endsection

@section('content')

    <div>
        <a href="{{route('admin.lesson.edit',[
                                'lesson'=>$lesson->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить эту урок?');" href="{{route('admin.lesson.delete',[
                                'lesson'=>$lesson->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр Урока: {{$lesson->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$lesson->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$lesson->title}}</td>
                    </tr>
                    <tr>
                        <th>Тип</th>
                        <td>{{$lesson->type}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{!! $lesson->context !!}</td>
                    </tr>

                    <tr>
                        <th>Превью</th>
                        <td>
                            @if($lesson->image_preview)
                                @image([
                                    'media' => $lesson->image_preview,
                                    'attrs' => 'style="max-width:300px;"'])

                            @else
                                Не загружено
                            @endif
                        </td>
                    </tr>
                    @if($lesson->video)
                    <tr>
                        <th>Видео</th>
                        <td>{!! $lesson->video !!}</td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
<form method="POST" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="status" class="col-md-4 col-form-label text-md-right">Статус</label>
        <div class="col-md-6">
            <select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" required>
                <option value="0" {{(old('status',$formData->status??0))==0?'selected':''}}> Черновик</option>
                <option value="1" {{(old('status',$formData->status??0))==1?'selected':''}}> Опубликовано</option>
            </select>
            @if ($errors->has('status'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="title" class="col-md-4 col-form-label text-md-right">Заголовок страницы</label>
        <div class="col-md-6">
            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title',$formData->title ?? '')  }}" required autofocus>
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">Описание</label>
        <div class="col-md-6">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ old('description',$formData->description ?? '') }}</textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="seo_keywords" class="col-md-4 col-form-label text-md-right">Ключевые слова</label>
        <div class="col-md-6">
            <input id="seo_keywords" type="text" class="form-control{{ $errors->has('seo_keywords') ? ' is-invalid' : '' }}" name="seo_keywords" value="{{ old('seo_keywords',$formData->seo_keywords ?? '')  }}" required autofocus>
            <span class="text-muted">Укажите через запятую</span>
            @if ($errors->has('seo_keywords'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('seo_keywords') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Тип страницы</label>
        <div class="col-md-6">
            <select id="type" type="text" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" required>
                <option value="олимпиада" {{(old('type')??$formData->type??'')=='олимпиада'?'selected':''}}> Олимпиада </option>
                <option value="курс" {{(old('type')??$formData->type??'')=='олимпиада'?'selected':''}}> Курс </option>
                <option value="прочее" {{(old('type')??$formData->type??'')=='прочее'?'selected':''}}> Прочее </option>
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="context" class="col-md-4 col-form-label text-md-right">Содержимое</label>
        <div class="col-md-12">
            <textarea id="context" class="form-control{{ $errors->has('context') ? ' is-invalid' : '' }}" name="context">{{ old('context',$formData->context ?? '') }}</textarea>
            @if ($errors->has('context'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('context') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="logo" class="col-md-4 col-form-label text-md-right">Логотип</label>
        <div class="col-md-6">
            <input type="file"  class="form-control custom-file-input{{ $errors->has('logo') ? ' is-invalid' : 'logo' }}" id="logo" name="logo" accept="image/jpeg,image/png">
            @if ($errors->has('logo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="cover_image" class="col-md-4 col-form-label text-md-right">Обложка</label>
        <div class="col-md-6">
            <input type="file"  class="form-control custom-file-input{{ $errors->has('cover_image') ? ' is-invalid' : 'cover_image' }}" id="cover_image" name="cover_image" accept="image/jpeg,image/png">
            @if ($errors->has('cover_image'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('cover_image') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <strong>{{ $errors->first() }}</strong>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
<script src='/adminLTE/js/tinymce/tinymce.min.js'></script>
<script>
    var dfreeBodyConfig = {
        selector: '#context',
        menubar: false,
        height:300,
        plugins: [
            "advlist anchor autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link media noneditable preview",
            " searchreplace table template textcolor visualblocks wordcount"
        ]
    };

    tinymce.init(dfreeBodyConfig);
</script>
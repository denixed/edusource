
@extends('layouts.admin',[
    'page_header'=>'Страницы'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li class="active">Страницы</li>
    </ol>
@endsection
@section('content')
    <div>
        <a href="{{route('admin.page.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить страницу</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->


        <div class="box-body">
            <form action="" class="form-inline">
                <div class="input-group">
                    <input type="search" class="form-control" placeholder="Поиск" name="s" value="{{\Request::get('s')}}">
                </div>
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search"></i>
                </button>

            </form>
            <div class="table-responsive">
                <table class="data-table table table-bordered table-striped">
                    <col width="auto" span="4">
                    <col width="150">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>Статус</th>
                        <th>Название</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $page)
                        <tr>
                            <th>{{$page->id}}</th>
                            <td>
                                @if($page->logo)
                                    <img style="max-width: 100px;" class="img-responsive" src="{{$page->logo}}" alt="">
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                {{$page->status==1?'Опубликовано':''}}
                                {{$page->status==0?'Черновик':''}}
                            </td>
                            <td>{{$page->title}}</td>
                            <td>
                                <a href="{{route('admin.page.read',[
                                    'page'=>$page->id
                                ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                <a href="{{route('admin.page.edit',[
                                    'page'=>$page->id
                                ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a onclick="return confirm('Вы действительно хотите удалить страницу #{{$page->id}}?');" href="{{route('admin.page.delete',[
                                    'page'=>$page->id
                                ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        {{$pages->appends(['s' => \Request::get('s')])->links()}}
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
@extends('layouts.admin',[
    'page_header'=>'Просмотр страницы'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.page.list')}}"> Страницы</a></li>
        <li class="active">{{$page->title}}</li>
    </ol>
@endsection

@section('content')

    <div>
        <a href="{{route('admin.page.edit',[
                                '$page'=>$page->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a href="{{route('admin.post.list',[
                                'parent_type'=>'Page',
                                'parent_id'=>$page->id
                            ])}}" class="btn btn-primary"><i class="fa fa-file-text-o"></i> &nbsp;Записи </a>
        <a onclick="return confirm('Вы действительно хотите удалить эту страницу?');" href="{{route('admin.page.delete',[
                                'page'=>$page->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр страницы: {{$page->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$page->id}}</td>
                    </tr>
                    <tr>
                        <th>Статус</th>
                        <td>
                            {{$page->status==1?'Опубликовано':''}}
                            {{$page->status==0?'Черновик':''}}
                        </td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$page->title}}</td>
                    </tr>
                    <tr>
                        <th>Ссылка</th>
                        <td>{{$page->link}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$page->description}}</td>
                    </tr>
                    <tr>
                        <th>Логотип</th>
                        <td>
                            @if($page->logo)
                                <img style="max-width: 150px;" class="img-responsive" src="{{$page->logo}}">
                            @else
                                Не загружен
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Фоновое изображение</th>
                        <td>
                            @if($page->cover_image)
                                <img style="max-width: 300px;" class="img-responsive" src="{{$page->cover_image}}">
                            @else
                                Не загружено
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Ключевые слова</th>
                        <td>{{$page->seo_keywords}}</td>
                    </tr>
                    <tr>
                        <th>Тип страницы</th>
                        <td>{{$page->type}}</td>
                    </tr>
                    <tr>
                        <th>Содержимое</th>
                        <td>{!!$page->context !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
@extends('layouts.admin',[
    'page_header'=>'Создать новую страницу'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.page.list')}}"> Страницы</a></li>
        <li class="active">Создать</li>
    </ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новую страницу </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/page/form',[
                'action'=>route('admin.page.store'),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
@endsection
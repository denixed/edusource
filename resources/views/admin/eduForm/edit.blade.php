@extends('layouts.admin',[
    'page_header'=>'Редактировать форму обучения'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать форму обучения </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/eduForm/form',[
                'action'=>route('admin.eduForm.update',[
                    'eduForm' => $eduForm->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $eduForm
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
@extends('layouts.admin',[
    'page_header'=>'Создать новую форму обучения'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новую форму обучения </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/eduForm/form',[
                'action'=>route('admin.eduForm.store'),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
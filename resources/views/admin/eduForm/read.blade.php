@extends('layouts.admin',[
    'page_header'=>'Просмотр формы обучения'
])

@section('content')

    <div>
        <a href="{{route('admin.eduForm.edit',[
                                'eduForm'=>$eduForm->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить эту форму обучения?');" href="{{route('admin.eduForm.delete',[
                                'eduForm'=>$eduForm->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр формы обучения {{$eduForm->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$eduForm->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$eduForm->name}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$eduForm->description}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
@extends('layouts.admin',[
    'page_header'=>'Просмотр прототипа'
])

@section('content')

    <div>
        <a href="{{route('admin.theme.list',[
                                'subject'=>$prot->theme->subject->id
                            ])}}" class="btn btn-primary"><i class="fa fa-backward"></i> &nbsp; Назад к темам </a>
        <a href="{{route('admin.prot.edit',[
                                'prot'=>$prot->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить этот прототип?');" href="{{route('admin.prot.delete',[
                                'prot'=>$prot->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр прототипа </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$prot->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$prot->name}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$prot->content}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
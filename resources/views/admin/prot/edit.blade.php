@extends('layouts.admin',[
    'page_header'=>'Редактировать прототип'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать прототип </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/prot/form',[
                'action'=>route('admin.prot.update',[
                    'prot' => $prot->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $prot
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
@extends('layouts.admin',[
    'page_header'=>'Создать новый прототип'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новый прототип</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/prot/form',[
                'action'=>route('admin.prot.store',[
                    'theme'=>$theme->id
                ]),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
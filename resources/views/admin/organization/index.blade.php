
@extends('layouts.admin',[
    'page_header'=>'Организации'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li class="active">Организации</li>
    </ol>
@endsection
@section('content')
    <div>
        <a href="{{route('admin.organization.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить организацию</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->

        @component('admin/organization/list',[
            'organizations'=>$organizations
        ])

        @endcomponent

    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- page script -->
    {{--<script>
        $(function () {
            $('.data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>--}}
@endsection
@extends('layouts.admin',[
    'page_header'=>'Просмотр организации'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li class="active">{{$organization->abbr}}</li>
    </ol>
@endsection
@section('content')
    <?php
        $vuz=$organization->vuz;
    ?>
    <div>
        <a href="{{route('admin.post.list',[
                                'parent_type'=>'Organization',
                                'parent_id'=>$organization->id
                            ])}}" class="btn btn-primary"><i class="fa fa-file-text-o"></i> &nbsp;Записи </a>


        <a href="{{route('admin.organization.edit',[
                                '$organization'=>$organization->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        @if($vuz)
            <a href="{{route('admin.organization.vuz.editProhodBalls',[
                                '$organization'=>$organization->id
                            ])}}" class="btn btn-primary"><i class="fa fa-star"></i> &nbsp; Проходные баллы </a>
            <a href="{{route('admin.vuz_division.list',[
                                'vuz'=>$organization->id
                            ])}}" class="btn btn-primary"> &nbsp; Подразделения</a>
        @endif

        <a onclick="return confirm('Вы действительно хотите удалить эту организацию?');" href="{{route('admin.organization.delete',[
                                '$organization'=>$organization->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр организации: {{$organization->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$organization->id}}</td>
                    </tr>
                    <tr>
                        <th>Статус</th>
                        <td>
                            {{$organization->status==1?'Опубликовано':''}}
                            {{$organization->status==0?'Черновик':''}}
                        </td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$organization->name}}</td>
                    </tr>
                    <tr>
                        <th>Краткое название (аббревиатура)</th>
                        <td>{{$organization->abbr}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$organization->description}}</td>
                    </tr>
                    <tr>
                        <th>Блок об организации</th>
                        <td>{!! $organization->about !!}</td>
                    </tr>
                    <tr>
                        <th>ФИО контактного лица</th>
                        <td>{{$organization->contact_full_name}}</td>
                    </tr>
                    <tr>
                        <th>Логотип</th>
                        <td>
                            @if($organization->logo_image)
                                <img style="max-width: 150px;" class="img-responsive" src="{{$organization->logo_image}}" alt="Логотип">
                            @else
                                Не загружен
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Регион</th>
                        <td>
                            {{$organization->region}}
                        </td>
                    </tr>
                    <tr>
                        <th>Адрес</th>
                        <td>
                            {{$organization->address}} <br>
                            <img src="https://static-maps.yandex.ru/1.x/?ll={{$organization->cord}}&size=450,300&z=15&l=map" alt="">
                        </td>
                    </tr>
                    <tr>
                        <th>Номер телефона</th>
                        <td>{{$organization->contact_number}}</td>
                    </tr>
                    <tr>
                        <th>Тип организации</th>
                        <td>{{$organization->type}}</td>
                    </tr>
                    @if($vuz)
                        <tr>
                            <th>Город</th>
                            <td>{{$vuz->city}}</td>
                        </tr>
                        <tr>
                            <th>Проходной балл</th>
                            <td>{{$vuz->min_ball}}</td>
                        </tr>
                        <tr>
                            <th>Бюджетных мест</th>
                            <td>{{$vuz->budget}}</td>
                        </tr>
                        <tr>
                            <th>Официальный сайт ВУЗа</th>
                            <td>{{$vuz->site}}</td>
                        </tr>
                        <tr>
                            <th>Общежитие</th>
                            <td>{{$vuz->hostel==1?'Есть':'Нет'}}</td>
                        </tr>
                        <tr>
                            <th>Военная кафедра</th>
                            <td>{{$vuz->military==1?'Есть':'Нет'}}</td>
                        </tr>
                        <tr>
                            <th>Специальности</th>
                            <td>
                                <table class="table table-bordered table-sm table-responsive">
                                    <thead>
                                        <tr>
                                            <th>НПС</th>
                                            <th>Специальность</th>
                                            <th>Факультет</th>
                                            <th>Кафедра</th>
                                            <th>Балл</th>
                                            <th>Необходимые предметы</th>
                                            <th>Год</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($organization->specialities as $speciality)
                                            @if(count($prohodBalls=$vuz->prohodBalls()->where('speciality_id',$speciality->id)->orderBy('year', 'desc')->orderBy('division_id', 'desc')->get()))

                                                    @foreach($prohodBalls as $index => $prohodBall)
                                                    <tr>
                                                        @if($index===0)
                                                            <td rowspan="{{count($prohodBalls) - $index}}">{{$speciality->nps}}</td>
                                                            <th rowspan="{{count($prohodBalls) - $index}}">{{$speciality->name}}</th>
                                                        @endif
                                                        <td>{{$prohodBall->division->vuz_faculty->short_name}}</td>
                                                        <td>{{$prohodBall->division->short_name}}. {{$prohodBall->division->name}}</td>
                                                        <td>{{$prohodBall->ball}}</td>
                                                        <td>{{$prohodBall->subjects}}</td>
                                                        <td>{{$prohodBall->year}}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th>Форма обучения</th>
                            <td>
                                <ul class="list-group">
                                    @foreach($organization->eduForms as $eduForms)
                                        <li class="list-group-item">{{$eduForms->name}}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
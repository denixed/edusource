<option value='{{$division->id}}' {{$division->id==$prohodBall->division_id?'selected':''}}>
    @for($i=0;$i<$level;$i++)&nbsp;&nbsp;&nbsp;&nbsp;@endfor
    {{$division->short_name}}.{{$division->name}}
</option>
@foreach( $division->children as $vuzDivision)
    @component('admin/organization/vuz/division_option',[
            'prohodBall'=>$prohodBall,
            'division'=>$vuzDivision,
            "level"=>$level+1
       ])
    @endcomponent
@endforeach
@extends('layouts.admin',[
    'page_header'=>'Заполнить проходные баллы'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li><a href="{{route('admin.organization.read',[
            'organization'=>$organization->id
        ])}}">{{$organization->abbr}}</a></li>
        <li class="active">Проходные баллы</li>
    </ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Заполнить проходные баллы</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{route('admin.organization.vuz.updateProhodBalls',[
                'organization'=> $organization->id
            ])}}", method="post">
                @csrf
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>НПС</th>
                            <th>Специальность</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(\App\Speciality::orderBy('nps')->get() as $speciality)
                            <tr>
                                <th>{{$speciality->nps}}</th>
                                <th>{{$speciality->name}}</th>
                            </tr>
                            <tr>
                                <td colspan="2" class="prohodBalls" speciality="{{$speciality->id}}">
                                        <div class="body">
                                        @foreach($organization->vuz->prohodBalls->where('speciality_id',$speciality->id) as $prohodBall)
                                                <div class="form-group form-inline">
                                                    <select class="form-control"  name="prohodBall[{{$prohodBall->id}}][division_id]">
                                                        <option value='null'>Не указан</option>
                                                        @foreach( $organization->vuz->divisions()->whereNull('parent')->get() as $vuzDivision)
                                                            @component('admin/organization/vuz/division_option',[
                                                                    'prohodBall'=>$prohodBall,
                                                                    'division'=>$vuzDivision,
                                                                    "level"=>0
                                                               ])
                                                            @endcomponent
                                                        @endforeach
                                                    </select>
                                                    <input class="form-control" name="prohodBall[{{$prohodBall->id}}][ball]" type="number" value="{{$prohodBall->ball}}" placeholder="Проходный балл">
                                                    <input class="form-control" name="prohodBall[{{$prohodBall->id}}][budgetplaces]" type="number" value="{{$prohodBall->budgetplaces}}" placeholder="Кол-во бюджетных мест">
                                                    <input class="form-control" name="prohodBall[{{$prohodBall->id}}][ballpayed]" type="number" value="{{$prohodBall->ballpayed}}" placeholder="Проходный балл на платной основе">
                                                    <input class="form-control" name="prohodBall[{{$prohodBall->id}}][paidplaces]" type="number" value="{{$prohodBall->paidplaces}}" placeholder="Кол-во мест на платной основе">
                                                    <input class="form-control" name="prohodBall[{{$prohodBall->id}}][subjects]" type="text" value="{{$prohodBall->subjects}}" placeholder="Предметы для сдачи" required>
                                                    <input class="form-control" name="prohodBall[{{$prohodBall->id}}][year]" type="number" value="{{$prohodBall->year}}" placeholder="Год" required>
                                                </div>
                                        @endforeach
                                        </div>
                                                <a class="addString btn btn-default" style="width: 100%;display: block;">Добавить строку</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-success btn-lg">Обновить данные</button>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <script>
        var i=0;
        document.addEventListener('DOMContentLoaded',function(){
           $('.addString').click(function(){
               var root_table=$(this).parents('.prohodBalls');
               root_table.find('.body').append(`
                    <div class="form-group form-inline">
                        <select class="form-control"  name="newprohodBall[`+i+`][division_id]">
                            <option value='null'>Не указан</option>
                            @foreach( $organization->vuz->divisions()->whereNull('parent')->get() as $vuzDivision)
                                   @component('admin/organization/vuz/division_option',[
                                           'prohodBall'=>$prohodBall,
                                           'division'=>$vuzDivision,
                                           "level"=>0
                                      ])
                                   @endcomponent
                            @endforeach
                        </select>
                        <input name="newprohodBall[`+i+`][speciality_id]" type="hidden" value="`+root_table.attr('speciality')+`">
                        <input class="form-control" name="newprohodBall[`+i+`][ball]" type="number" value="" placeholder="Проходный балл">
                        <input class="form-control" name="newprohodBall[`+i+`][budgetplaces]" type="number" value="" placeholder="Кол-во бюджетных мест">
                        <input class="form-control" name="newprohodBall[`+i+`][ballpayed]" type="number" value="" placeholder="Проходный балл на платной основе">
                        <input class="form-control" name="newprohodBall[`+i+`][paidplaces]" type="number" value="" placeholder="Кол-во мест на платной основе">
                        <input class="form-control" name="newprohodBall[`+i+`][subjects]" type="text" value="" placeholder="Предметы для сдачи" required>
                        <input class="form-control" name="newprohodBall[`+i+`][year]" type="number" value="" placeholder="Год" required>
                    </div>
               `);
               i++;
           })
        });
    </script>
@endsection
<form method="POST" action="{{$action}}" enctype="multipart/form-data">
    @csrf

    @if(isset($formData))
        <div class="form-group row">
            <label for="type" class="col-md-4 col-form-label text-md-right">Статус</label>
            <div class="col-md-6">
                <select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" required>
                    <option value="0" {{(old('status')??$formData->status)==0?'selected':''}}> Черновик </option>
                    <option value="1" {{(old('status')??$formData->status)==1?'selected':''}}> Опубликовано </option>
                </select>
                @if ($errors->has('status'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
            </div>
        </div>
    @else
        <input type="hidden" name="status" value="0">
    @endif
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Название организации</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$formData->name ?? '')  }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="abbr" class="col-md-4 col-form-label text-md-right">Краткое название (аббревиатура)</label>
        <div class="col-md-6">
            <input id="abbr" type="text" class="form-control{{ $errors->has('abbr') ? ' is-invalid' : '' }}" name="abbr" value="{{ old('abbr',$formData->abbr ?? '')  }}" required autofocus>
            @if ($errors->has('abbr'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('abbr') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">Описание организации</label>
        <div class="col-md-6">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ old('description',$formData->description ?? '') }}</textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="about" class="col-md-4 col-form-label text-md-right">Блок об организации</label>
        <div class="col-md-12">
            <textarea id="about" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}" name="about">{{ old('about',$formData->about ?? '') }}</textarea>
            @if ($errors->has('about'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('about') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="region" class="col-md-4 col-form-label text-md-right">Регион</label>
        <div class="col-md-6">
            <input id="region" type="text" class="form-control{{ $errors->has('region') ? ' is-invalid' : '' }}" name="region" value="{{ old('region',$formData->region ?? '')  }}">
            @if ($errors->has('region'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('region') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">Адрес организации</label>
        <div class="col-md-6">
            <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address',$formData->address ?? '')  }}">
            @if ($errors->has('address'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif
            @if ($errors->has('cord'))
                <span class="invalid-feedback" role="alert">
                    <strong>Адрес не корректен</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="address" class="col-md-4 col-form-label text-md-right">Карта</label>
        <div class="col-md-6">
            <div id="map_canvas" style="height: 300px;"></div>
            <input id="cord" type="hidden" name="cord">
        </div>
    </div>
    <div class="form-group row">
        <label for="contact_full_name" class="col-md-4 col-form-label text-md-right">ФИО контактного лица</label>
        <div class="col-md-6">
            <input id="contact_full_name" type="text" class="form-control{{ $errors->has('contact_full_name') ? ' is-invalid' : '' }}" name="contact_full_name" value="{{ old('contact_full_name',$formData->contact_full_name ?? '') }}" required>
            @if ($errors->has('contact_full_name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('contact_full_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="contact_number" class="col-md-4 col-form-label text-md-right">Контактный номер телефона</label>
        <div class="col-md-6">
            <input id="contact_number" type="number" class="form-control{{ $errors->has('contact_number') ? ' is-invalid' : '' }}" name="contact_number" value="{{ old('contact_number',$formData->contact_number ?? '') }}" required>
            @if ($errors->has('contact_number'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('contact_number') }}</strong>
                </span>
            @endif
        </div>
    </div>
    @if(!(isset($formData)&&$formData&&$formData->type!=''))
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Тип организации</label>
        <div class="col-md-6">
            <select id="type" type="text" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" required>
                <option disabled {{!old('type')?'selected':''}}> Выберите тип организации </option>
                <option value="вуз" {{old('type')=='вуз'?'selected':''}}> ВУЗ </option>
                <option value="курсы" {{old('type')=='курсы'?'selected':''}}> Курсы дополнительной подготовки </option>
                <option value="0" {{old('type')=='0'?'selected':''}}> Другое </option>
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <input type="hidden" name="logo_image" value="0">
    @else
        <input type="hidden" name="type" value="{{$formData->type}}">
        <div class="form-group row">
            <label for="logo" class="col-md-4 col-form-label text-md-right">Логотип организации</label>
            <div class="col-md-6">
                <input type="file"  class="form-control custom-file-input{{ $errors->has('logo') ? ' is-invalid' : 'logo' }}" id="logo" name="logo" accept="image/jpeg,image/png">
                @if ($errors->has('logo'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
                @endif
            </div>
        </div>
    @endif

    @if(isset($formData)&&$vuz=$formData->vuz)
        <div class="form-group row">
            <label for="city" class="col-md-4 col-form-label text-md-right">Город ВУЗа</label>
            <div class="col-md-6">
                <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city',$vuz->city ?? '')  }}" required>
                @if ($errors->has('address'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
                @endif
                @if ($errors->has('cord'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="min_ball" class="col-md-4 col-form-label text-md-right">Проходной балл</label>
            <div class="col-md-6">
                <input id="min_ball" type="number" class="form-control{{ $errors->has('min_ball') ? ' is-invalid' : '' }}" name="min_ball" value="{{ old('min_ball',$vuz->min_ball ?? '')  }}" required>
                @if ($errors->has('min_ball'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('min_ball') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="budget" class="col-md-4 col-form-label text-md-right">Бюджетных мест</label>
            <div class="col-md-6">
                <input id="budget" type="number" class="form-control{{ $errors->has('budget') ? ' is-invalid' : '' }}" name="budget" value="{{ old('budget',$vuz->budget ?? '')  }}" required>
                @if ($errors->has('budget'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('budget') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="site" class="col-md-4 col-form-label text-md-right">Официальный сайт ВУЗа</label>
            <div class="col-md-6">
                <input id="site" type="url" class="form-control{{ $errors->has('site') ? ' is-invalid' : '' }}" name="site" value="{{ old('site',$vuz->site ?? '')  }}" required>
                @if ($errors->has('site'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('site') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="hostel" class="col-md-4 col-form-label text-md-right">Общежитие</label>
            <div class="col-md-6">
                <select id="hostel" type="text" class="form-control{{ $errors->has('hostel') ? ' is-invalid' : '' }}" name="hostel" required>
                    <option disabled {{!(old('hostel')??$vuz->hostel)?'selected':''}}> Укажите наличие общежития </option>
                    <option value="1" {{(old('hostel')??$vuz->hostel)===1?'selected':''}}> Есть </option>
                    <option value="0" {{(old('hostel')??$vuz->hostel)===0?'selected':''}}> Нет </option>
                </select>
                @if ($errors->has('hostel'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('hostel') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="military" class="col-md-4 col-form-label text-md-right">Военная кафедра</label>
            <div class="col-md-6">
                <select id="military" type="text" class="form-control{{ $errors->has('military') ? ' is-invalid' : '' }}" name="military" required>
                    <option disabled {{!(old('military')??$vuz->military)?'selected':''}}> Укажите наличие военной кафедры </option>
                    <option value="1" {{(old('military')??$vuz->military)===1?'selected':''}}> Есть </option>
                    <option value="0" {{(old('military')??$vuz->military)===0?'selected':''}}> Нет </option>
                </select>
                @if ($errors->has('military'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('military') }}</strong>
                </span>
                @endif
            </div>
        </div>

         <!--<div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">Специальности</label>
            <div class="col-md-6">
                @foreach(App\Speciality::all() as $speciality)
                <div class="form-check">
                    <?php
                        $check = $formData->hasSpeciality($speciality->id)==true;
                        if(old('specialities')){
                            if(isset(old('specialities')[$speciality->id])) $check= true;
                            else $check = false;
                        }
                    ?>
                    <input {{$check?'checked':''}} class="form-check-input" type="checkbox" value="1" name="specialities[{{$speciality->id}}]" id="specialityCheck{{$speciality->id}}">
                    <label class="form-check-label" for="specialityCheck{{$speciality->id}}">
                        {{$speciality->nps}}.{{$speciality->name}}
                    </label>
                </div>
                @endforeach

            </div>
        </div>
        --->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">Формы обучения</label>
            <div class="col-md-6">
                @foreach(App\EduForm::all() as $eduForm)
                    <div class="form-check">
                        <?php
                        $check = $formData->hasEduForm($eduForm->id)==true;
                        if(old('eduForms')){
                            if(isset(old('eduForms')[$eduForm->id])) $check= true;
                            else $check = false;
                        }
                        ?>
                        <input {{$check?'checked':''}} class="form-check-input" type="checkbox" value="1" name="eduForms[{{$eduForm->id}}]" id="eduFormCheck{{$eduForm->id}}">
                        <label class="form-check-label" for="eduFormCheck{{$eduForm->id}}">
                            {{$eduForm->name}}
                        </label>
                    </div>
                @endforeach

            </div>
        </div>

    @endif

    <input type="hidden" name="additional" value="0">

    <strong>{{ $errors->first() }}</strong>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
<script src='/adminLTE/js/tinymce/tinymce.min.js'></script>
<script>
    var dfreeBodyConfig = {
        selector: '#about',
        menubar: false,
        height:300,
        plugins: [
            "advlist anchor autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link media noneditable preview",
            " searchreplace table template textcolor visualblocks wordcount"
        ]
    };

    tinymce.init(dfreeBodyConfig);
</script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>

    ymaps.ready(init_maps);
    function init_maps() {
        var myMap = new ymaps.Map('map_canvas', {
            center: [0, 0],
            zoom: 16,
            controls: ['zoomControl','typeSelector',  'fullscreenControl']
        });
        var myCollection = new ymaps.GeoObjectCollection({}, {
            preset: 'islands#violetStretchyIcon', //все метки красные
            draggable: false, // и их можно перемещать
        });
        // добавляем коллекцию на карту
        myMap.geoObjects.add(myCollection);
        var myPlacemark;
        var change_point=function(){
            try {
                myCollection.remove(myPlacemark);
            }catch (e) {}
            $('#cord').val('');
            $.getJSON('https://geocode-maps.yandex.ru/1.x/', {
                'format': 'json',
                'geocode': $('#address').val()
            },function(d){


                var pos=d.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(' ').reverse();
                myPlacemark = new ymaps.Placemark(pos, {
                    iconContent: '',
                    balloonContent: ''
                }, {
                    preset: 'islands#violetStretchyIcon'
                });
                myCollection.add(myPlacemark);
                myMap.setCenter(pos);
                $('#cord').val(pos.reverse().join(','));
            });
        };
        change_point();
        $('#address').on('change',change_point);
        $('#address').on('keydown',change_point);
    };


</script>
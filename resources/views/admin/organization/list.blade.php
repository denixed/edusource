<div class="box-body">
    <form action="" class="form-inline">
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Поиск" name="s" value="{{\Request::get('s')}}">
        </div>
        <button class="btn btn-primary" type="submit">
            <i class="fa fa-search"></i>
        </button>

    </form>
    <div class="table-responsive">
        <table class="data-table table table-bordered table-striped">
            <col width="auto" span="4">
            <col width="150">
            <thead>
            <tr>
                <th>#</th>
                <th></th>
                <th>Статус</th>
                <th>Название</th>
                <th>Тип</th>
            </tr>
            </thead>
            <tbody>
            @foreach($organizations as $organization)
                <tr>
                    <th>{{$organization->id}}</th>
                    <td>
                        @if($organization->logo_image)
                            <img style="max-width: 75px;" class="img-responsive" src="{{$organization->logo_image}}" alt="Логотип">
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        {{$organization->status==1?'Опубликовано':''}}
                        {{$organization->status==0?'Черновик':''}}
                    </td>
                    <td>
                        <a href="{{route('admin.organization.read',[
                                    'organization'=>$organization->id
                            ])}}">{{$organization->abbr}}</a>
                    </td>
                    <td>{{$organization->type}}</td>
                    <td>@if($organization->type=='вуз')
                            {{$organization->vuz->divisions()->count()}}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- /.box-body -->
{{$organizations->appends(['s' => \Request::get('s')])->links()}}
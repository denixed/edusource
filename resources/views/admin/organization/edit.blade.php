@extends('layouts.admin',[
    'page_header'=>'Редактировать организацию'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li><a href="{{route('admin.organization.read',[
            'organization'=>$organization->id
        ])}}">{{$organization->abbr}}</a></li>
        <li class="active">Редактирование</li>
    </ol>
@endsection
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать организацию </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/organization/form',[
                'action'=>route('admin.organization.update',[
                    'organization' => $organization->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $organization
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
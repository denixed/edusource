@extends('layouts.admin',[
    'page_header'=>'Редактировать предмет'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать предмет </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/subject/form',[
                'action'=>route('admin.subject.update',[
                    'subject' => $subject->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $subject
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
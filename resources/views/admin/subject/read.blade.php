@extends('layouts.admin',[
    'page_header'=>'Просмотр предмета'
])

@section('content')

    <div>
        <a href="{{route('admin.theme.list',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-primary"><i class="fa fa-list"></i> &nbsp; Темы </a>
        <a href="{{route('admin.subject.edit',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить этот предмет?');" href="{{route('admin.subject.delete',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр предмета {{$subject->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$subject->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$subject->name}}</td>
                    </tr>
                    <tr>
                        <th>Экзамен</th>
                        <td>{{$subject->exam->name}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
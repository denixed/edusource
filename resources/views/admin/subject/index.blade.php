@extends('layouts.admin',[
    'page_header'=>('Предметы '.$exam->name)
])

@section('content')
    <div>
        <a href="{{route('admin.subject.create',[
            'exam'=>$exam->id
        ])}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить предмет {{$exam->name}}</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="data-table table table-bordered table-striped">
                <col width="auto" span="4">
                <col width="150">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($subjects as $subject)
                    <tr>
                        <th>{{$subject->id}}</th>
                        <td>{{$subject->name}}</td>
                        <td>
                            <a href="{{route('admin.theme.list',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-warning"><i class="fa fa-list"></i></a>
                            <a href="{{route('admin.subject.read',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                            <a href="{{route('admin.subject.edit',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <a onclick="return confirm('Вы действительно хотите удалить предмет #{{$subject->name}}?');" href="{{route('admin.subject.delete',[
                                'subject'=>$subject->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
            $('.data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>
@endsection
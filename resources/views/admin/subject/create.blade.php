@extends('layouts.admin',[
    'page_header'=>'Создать новый предмет'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новый предмет </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/subject/form',[
                'action'=>route('admin.subject.store',[
                    'exam'=>$exam->id
                ]),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
<?php
    if($media) $media = App\Media::find($media);
    if(!$media){
        $media = new App\Media();
    }
    //dd($media);
?>
<div>
    <div class="alert alert-success" id="successUploadMessage{{$formName}}" style="display: none;">
        <h4>Файл успешно загружен</h4>
    </div>
    <img src="{{$media->url}}" alt="{{$media->title}}" id="uploadedImage{{$formName}}" width="300">
    <p>
        <small id="uploadedImageTitle{{$formName}}">
            {{$media->title}}
        </small>
    </p>
    <input type="hidden" name="{{$formName}}" value="{{$media->id}}" >
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadForm{{$formName}}"> &nbsp; Загрузить файл</button>

    @if ($errors->has($formName))
        <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first($formName) }}</strong>
                </span>
    @endif
</div>
<!-- Large modal -->

<div class="modal fade" id="uploadForm{{$formName}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel{{$formName}}">Загрузка файлов</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="titleFileInput">Название файла</label>
                    <input type="text" class="form-control" id="titleFileInput{{$formName}}" placeholder="Название файла">
                </div>
                <div class="form-group">
                    <label for="fileInput">Файл</label>
                    <input type="file" id="fileInput{{$formName}}" accept="image/jpeg, image/png, image/bmp">
                    <p class="help-block"> Поддерживаемые форматы: jpq, png, bmp.</p>
                </div>

                <div class="preview">

                </div>

                <button type="button" class="btn btn-success" id="fileUploadButton{{$formName}}"> Загрузить </button>

            </div>
        </div>
    </div>
</div>


<script>
    document.addEventListener('DOMContentLoaded',function(){
        var upload_url = "{{route('admin.media.store')}}";
        var file_list_url = "{{route('admin.media.list')}}";
        $('#fileInput{{$formName}}').change(function(){
            if(this.files.length){
                var file = this.files[0];
                var blob = file.slice(0, file.size, file.type);
                var image = new Image();
                image.src = URL.createObjectURL(blob);
                image.style.maxWidth="300px";
                image.style.maxHeight="300px";
                image.onload= function(){
                    if(!$('#titleFileInput{{$formName}}').val()) $('#titleFileInput{{$formName}}').val(file.name.split('.')[0]);
                    $('#uploadForm{{$formName}} .preview').html(image);
                };
            }else{
                $('#uploadForm{{$formName}} .preview').html('');
            }

        });
        $('#fileUploadButton{{$formName}}').click(function(){
            if(!document.getElementById('fileInput{{$formName}}').files.length){
                return alert('Выберите файл!');
            }
            if(!$('#titleFileInput{{$formName}}').val()){
                return alert('Заполните поле название файла!');
            }
            var file = document.getElementById('fileInput{{$formName}}').files[0];
            var title=$('#titleFileInput{{$formName}}').val();
            var data = new FormData();
            data.set('file',file);
            data.set('title',title);

            jQuery.ajax({
                url: upload_url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data){
                    $('#uploadForm{{$formName}}').modal('hide');
                    $('#fileInput{{$formName}}').val('').change();
                    $('#titleFileInput{{$formName}}').val('');
                    $('#uploadedImage{{$formName}}').attr('src', data.url);
                    $('[name="{{$formName}}"').attr('value', data.id);
                    $('#uploadedImageTitle{{$formName}}').text(data.title);
                    $('#successUploadMessage{{$formName}}').slideDown();
                    setTimeout(function () {
                        $('#successUploadMessage{{$formName}}').slideUp();
                    },5000);
                },
                error:function(err){
                    alert('Произошла ошибка загрузки');
                }
            });
        });
    });

</script>
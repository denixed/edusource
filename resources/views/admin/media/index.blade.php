@extends('layouts.admin',[
    'page_header'=>'Медиа файлы'
])


@section('content')
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadForm"> &nbsp; Загрузить файл</button>
    </div>
    <!-- Large modal -->

    <div class="modal fade" id="uploadForm" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Загрузка файлов</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="titleFileInput">Название файла</label>
                        <input type="email" class="form-control" id="titleFileInput" placeholder="Название файла" required>
                    </div>
                    <div class="form-group">
                        <label for="fileInput">Файл</label>
                        <input type="file" id="fileInput" accept="image/jpeg, image/png, image/bmp">
                        <p class="help-block"> Поддерживаемые форматы: jpq, png, bmp.</p>
                    </div>

                    <div class="preview">

                    </div>

                    <button type="button" class="btn btn-success" id="fileUploadButton"> Загрузить </button>

                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="box" id="files_box">
        <div class="box-header">
            <h3 class="box-title">Файлы</h3>
            <button class="btn btn-default right" onclick="reload_files();" style="float: right;">
                <i class="fa fa-refresh"></i>
            </button>
        </div>

        <div class="alert alert-success" id="successUploadMessage" style="display: none;">
            <h4>Файл успешно загружен</h4>
        </div>

        <ul class="pagination" role="navigation">
        </ul>
        <!-- /.box-header -->
        <div class="box-body" id="files_container">
            Загрузка страницы ...
        </div>
        <ul class="pagination" role="navigation">

        </ul>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <script>
        var upload_url = "{{route('admin.media.store')}}";
        var file_list_url = "{{route('admin.media.list')}}";
        var current_page=1;
        $('#fileInput').change(function(){
            if(this.files.length){
                var file = this.files[0];
                var blob = file.slice(0, file.size, file.type);
                var image = new Image();
                image.src = URL.createObjectURL(blob);
                image.style.maxWidth="300px";
                image.style.maxHeight="300px";
                image.onload= function(){
                    if(!$('#titleFileInput').val()) $('#titleFileInput').val(file.name.split('.')[0]);
                    $('#uploadForm .preview').html(image);
                };
            }else{
                $('#uploadForm .preview').html('');
            }

        });
        $('#fileUploadButton').click(function(){
            if(!document.getElementById('fileInput').files.length){
                return alert('Выберите файл!');
            }
            if(!$('#titleFileInput').val()){
                return alert('Заполните поле название файла!');
            }
            var file = document.getElementById('fileInput').files[0];
            var title=$('#titleFileInput').val();
            var data = new FormData();
            data.set('file',file);
            data.set('title',title);

            jQuery.ajax({
                url: upload_url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data){
                    $('#uploadForm').modal('hide');
                    $('#fileInput').val('').change();
                    $('#titleFileInput').val('');
                    current_page=1;
                    reload_files();
                    $('#successUploadMessage').slideDown();
                    setTimeout(function () {
                        $('#successUploadMessage').slideUp();
                    },5000);
                },
                error:function(err){
                    alert('Произошла ошибка загрузки');
                }
            });
        });
        function reload_files(){
            if(current_page<=0) current_page=1;
            $('#files_container').html('Идет загрузка файлов ...');
            $.getJSON(file_list_url,{
                page: current_page
            },function (data) {
                if(!data.data.length){
                    if(current_page>data.meta.last_page){
                        current_page=data.meta.last_page;
                        return reload_files();
                    }
                    $('#files_container').html("<div class=\"alert alert-info\">\n" +
                        "                <h4>\n" +
                        "                    Файлы не найдены\n" +
                        "                </h4>\n" +
                        "                <p>В системе нет ещё ни одного файла.</p>\n" +
                        "            </div>");
                    return null;
                }
                $('#files_container').html('<div class="flex files_block"></div>');
                var block = $('#files_container .flex.files_block');
                data.data.forEach(function(file){
                    if(file.type==1){
                        var img;
                        if(file.children.length) img = file.children[0];
                        else img = file;
                        block.append('<div class="card">\n' +
                            '                    <img src="'+img.url+'" class="card-img-top" alt="'+img.title+'">\n' +
                        '                    <div class="card-body">\n' +
                        '                        <h5 class="card-title">'+img.title+'</h5>\n' +
                        '                        <!--<a href="#" class="btn btn-primary">Редактировать</a>-->\n' +
                        '                    </div>\n' +
                        '                </div>')
                    }else{
                        block.append('<div class="card">\n' +
                            '                    <div class="card-body">\n' +
                            '                        <h5 class="card-title">'+file.title+'</h5>\n' +
                            '                        <a href="'+file.url+'" class="btn btn-primary">Открыть</a>\n' +
                            '                    </div>\n' +
                            '                </div>')
                    }

                });

                var pag = $('#files_box .pagination');
                pag.html('<li class="page-item" >\n' +
                    '                <a class="page-link" rel="back" aria-label="« Назад" onclick="current_page--;reload_files();">‹</a>\n' +
                    '            </li>\n');
                for(var i=1;i<=data.meta.last_page;i++){
                    pag.append('<li class="page-item '+((i===current_page)?'active':'')+'"><a class="page-link" onclick="current_page='+i+';reload_files();">'+i+'</a></li>\n');
                }
                pag.append('<li class="page-item" >\n' +
                    '         <a class="page-link" rel="next" aria-label="Вперёд »" onclick="current_page++;reload_files();">›</a>\n' +
                    '       </li>');



            }).fail(function(err){
                $('#files_container').html("<div class=\"alert alert-danger\">\n" +
                    "                <h4>\n" +
                    "                    <i class=\"icon fa fa-ban\"></i>\n" +
                    "                    Произошла ошибка загрузки!\n" +
                    "                </h4>\n" +
                    "                <p>Попробуйте обновить страницу! Если проблем не решается, обратитесь к адмнистратору системы.</p>\n" +
                    "            </div>");
            });
        }
        $(document).ready(function(){
            reload_files();
        });

    </script>

    <style>
        .card {
            box-shadow: 1px 1px 4px #ccc;
            margin: 10px;
        }

        img.card-img-top {
            width: 100%;
        }

        .card-body {
            padding: 10px;
        }
        .flex{
            display: flex;
            flex-wrap: wrap;
            justify-content: space-evenly;
        }
        .page-link{
            cursor: pointer;
        }
    </style>

    <style>
        .card{
            max-width: 200px;
        }
    </style>

@endsection
@extends('layouts.admin',[
    'page_header'=>'Создать новую запись'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новую запись </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/post/form',[
                'action'=>route('admin.post.store',[
                    'parent_type'=>$parent_type,
                    'parent_id'=>$parent_id
                ]),
                'formSubmitTitle' => 'Создать',
                'parent_type'=>$parent_type,
                'parent_id'=>$parent_id
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
@endsection

@extends('layouts.admin',[
    'page_header'=>'Записи'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        @if($parent_type==='Page')
            <li><a href="{{route('admin.page.list')}}"> Страницы</a></li>
            <li><a href="{{route('admin.page.read',[
            'page'=>$parent->id
        ])}}">{{$parent->title}}</a></li>
        @elseif($parent_type=='Organization')
            <li><a href="{{route('admin.organization.list')}}"> Организации</a></li>
            <li><a href="{{route('admin.organization.read',[
            'page'=>$parent->id
        ])}}">{{$parent->abbr}}</a></li>
        @endif
        <li class="active">Записи</li>
    </ol>
@endsection

@section('content')
    <div>
        <a href="{{route('admin.post.create',[
            'parent_type'=>$parent_type,
            'parent_id'=>$parent_id
        ])}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить запись</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->


        <div class="box-body">
            <form action="" class="form-inline">
                <div class="input-group">
                    <input type="search" class="form-control" placeholder="Поиск" name="s" value="{{\Request::get('s')}}">
                </div>
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search"></i>
                </button>

            </form>
            <div class="table-responsive">
                <table class="data-table table table-bordered table-striped">
                    <col width="auto" span="4">
                    <col width="150">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>Статус</th>
                        <th>Название</th>
                        <th>Дата публикации</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <th>{{$post->id}}</th>
                            <td>
                                @if($post->preview_photo)
                                    <img style="max-width: 100px;" class="img-responsive" src="{{$post->preview_photo}}" alt="">
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                {{$post->status==1?'Опубликовано':''}}
                                {{$post->status==0?'Черновик':''}}
                            </td>
                            <td>{{$post->title}}</td>
                            <td>{{$post->created_at}}</td>
                            <td>
                                <a href="{{route('admin.post.read',[
                                    'post'=>$post->id
                                ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                <a href="{{route('admin.post.edit',[
                                    'post'=>$post->id
                                ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a onclick="return confirm('Вы действительно хотите удалить запись #{{$post->id}}?');" href="{{route('admin.post.delete',[
                                    'post'=>$post->id
                                ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        {{$posts->appends(['s' => \Request::get('s')])->links()}}
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- page script -->
    {{--<script>
        $(function () {
            $('.data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>--}}
@endsection
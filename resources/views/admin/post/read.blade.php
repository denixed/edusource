@extends('layouts.admin',[
    'page_header'=>'Просмотр записи'
])

@section('content')

    <div>
        <a href="{{route('admin.post.edit',[
                                '$post'=>$post->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить эту организацию?');" href="{{route('admin.post.delete',[
                                'post'=>$post->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр записи: {{$post->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$post->id}}</td>
                    </tr>
                    <tr>
                        <th>Статус</th>
                        <td>
                            {{$post->status==1?'Опубликовано':''}}
                            {{$post->status==0?'Черновик':''}}
                        </td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$post->title}}</td>
                    </tr>
                    <tr>
                        <th>Ссылка</th>
                        <td>{{$post->link}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$post->description}}</td>
                    </tr>
                    <tr>
                        <th>Превью</th>
                        <td>
                            @if($post->preview_photo)
                                <img style="max-width: 150px;" class="img-responsive" src="{{$post->preview_photo}}">
                            @else
                                Не загружено
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Ключевые слова</th>
                        <td>{{$post->seo_keywords}}</td>
                    </tr>
                    <tr>
                        <th>Категории</th>
                        <td>{{$post->tags}}</td>
                    </tr>
                    <tr>
                        <th>Тип записи</th>
                        <td>{{$post->type}}</td>
                    </tr>
                    <tr>
                        <th>Содержимое</th>
                        <td>{!!$post->context !!}</td>
                    </tr>
                    <tr>
                        <th>Рекомендации</th>
                        <td>
                            {{$post->show_in_feed==1?'Показывать в рекомендациях':''}}
                            {{$post->show_in_feed==0?'Не показывать в рекомендациях':''}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
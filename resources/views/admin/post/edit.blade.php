@extends('layouts.admin',[
    'page_header'=>'Редактировать запись'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать запись </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/post/form',[
                'action'=>route('admin.post.update',[
                    'post' => $post->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $post
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
@endsection
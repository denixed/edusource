<form method="POST" action="{{$action}}" enctype="multipart/form-data">
    @csrf

    @if(isset($formData))
        <div class="form-group row">
            <label for="type" class="col-md-4 col-form-label text-md-right">Статус</label>
            <div class="col-md-6">
                <select id="status" type="text" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" required>
                    <option value="0" {{(old('status')??$formData->status)==0?'selected':''}}> Черновик </option>
                    <option value="1" {{(old('status')??$formData->status)==1?'selected':''}}> Опубликовано </option>
                </select>
                @if ($errors->has('status'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
                @endif
            </div>
        </div>
    @else
        <input type="hidden" name="status" value="0">
    @endif
    <div class="form-group row">
        <label for="title" class="col-md-4 col-form-label text-md-right">Заголовок записи</label>
        <div class="col-md-6">
            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title',$formData->title ?? '')  }}" required autofocus>
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="link" class="col-md-4 col-form-label text-md-right">Ссылка на запись <span class="text-muted">(латиницей)</span></label>
        <div class="col-md-6">
            <input id="link" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" name="link" value="{{ old('link',$formData->link ?? '')  }}" required autofocus>
            <a id="generateLink" class="btn btn-primary">Сгенерироровать</a>
            @if ($errors->has('link'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('link') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">Описание</label>
        <div class="col-md-6">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ old('description',$formData->description ?? '') }}</textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="seo_keywords" class="col-md-4 col-form-label text-md-right">Ключевые слова</label>
        <div class="col-md-6">
            <input id="seo_keywords" type="text" class="form-control{{ $errors->has('seo_keywords') ? ' is-invalid' : '' }}" name="seo_keywords" value="{{ old('seo_keywords',$formData->seo_keywords ?? '')  }}" required autofocus>
            <span class="text-muted">Укажите через запятую</span>
            @if ($errors->has('seo_keywords'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('seo_keywords') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="tags" class="col-md-4 col-form-label text-md-right">Категории</label>
        <div class="col-md-6">
            <input id="tags" type="text" class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}" name="tags" value="{{ old('tags',$formData->tags ?? '')  }}" required autofocus>
            <span class="text-muted">Укажите через запятую</span>
            @if ($errors->has('tags'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('tags') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Тип запись</label>
        <div class="col-md-6">
            <select id="type" type="text" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" required>
                <option value="новость" {{(old('type')??$formData->type??'')=='новость'?'selected':''}}> Новость </option>
                <option value="статья" {{(old('type')??$formData->type??'')=='статья'?'selected':''}}> Полезная статья </option>
                <option value="объявление" {{(old('type')??$formData->type??'')=='объявление'?'selected':''}}> Объявление </option>
                <option value="прочее" {{(old('type')??$formData->type??'')=='прочее'?'selected':''}}> Прочее </option>
            </select>
            @if ($errors->has('type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="context" class="col-md-4 col-form-label text-md-right">Содержимое</label>
        <div class="col-md-12">
            <textarea id="context" class="form-control{{ $errors->has('context') ? ' is-invalid' : '' }}" name="context">{{ old('context',$formData->context ?? '') }}</textarea>
            @if ($errors->has('context'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('context') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="preview_photo" class="col-md-4 col-form-label text-md-right">Изображение записи</label>
        <div class="col-md-6">
            <input type="file"  class="form-control custom-file-input{{ $errors->has('preview_photo') ? ' is-invalid' : 'preview_photo' }}" id="preview_photo" name="preview_photo" accept="image/jpeg,image/png">
            @if ($errors->has('preview_photo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('preview_photo') }}</strong>
                </span>
            @endif
        </div>
    </div>
    @if(isset($formData)&&$formData)
        <div class="form-group row">
            <label for="type" class="col-md-4 col-form-label text-md-right">Рекомендации</label>
            <div class="col-md-6">
                <select id="show_in_feed" type="text" class="form-control{{ $errors->has('show_in_feed') ? ' is-invalid' : '' }}" name="show_in_feed" required>
                    <option value="0" {{(old('show_in_feed')??$formData->show_in_feed??'')==0?'selected':''}}> Не показывать в рекомендациях </option>
                    <option value="1" {{(old('show_in_feed')??$formData->show_in_feed??'')==1?'selected':''}}> Показывать в рекомендациях </option>
                </select>
                @if ($errors->has('show_in_feed'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('show_in_feed') }}</strong>
                </span>
                @endif
            </div>
        </div>
    @endif
    <strong>{{ $errors->first() }}</strong>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
<script src='/adminLTE/js/tinymce/tinymce.min.js'></script>
<script>
    var dfreeBodyConfig = {
        selector: '#context',
        menubar: false,
        height:300,
        plugins: [
            "advlist anchor autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link media noneditable preview",
            " searchreplace table template textcolor visualblocks wordcount"
        ]
    };

    tinymce.init(dfreeBodyConfig);
    document.addEventListener('DOMContentLoaded',function(){
       $('#generateLink').click(function(){
           $('#link').val(translit($('#title').val()));
       });
       $('#link').keydown(function(){
           this.value=translit(this.value);
       });
    });
    function translit(text_var){
        var space = '-';
        var text = text_var.toLowerCase();
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'kh',
            'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh','ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
            ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
            '#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
            '(': space, ')': space,'-': space, '\=': space, '+': space, '[': space,
            ']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
            '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
            '?': space, '<': space, '>': space, '№':space
        };
        var result = '';
        var curent_sim = '';
        for(i=0; i < text.length; i++) {
            if(transl[text[i]] != undefined) {
                if(curent_sim != transl[text[i]] || curent_sim != space){
                    result += transl[text[i]];
                    curent_sim = transl[text[i]];
                }
            }
            else {
                result += text[i];
                curent_sim = text[i];
            }
        }

        return TrimStr(result);
    }
    function TrimStr(s) {
        s = s.replace(/^-/, '');
        return s.replace(/-$/, '');
    }
</script>
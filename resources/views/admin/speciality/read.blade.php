@extends('layouts.admin',[
    'page_header'=>'Просмотр специальности'
])

@section('content')

    <div>
        <a href="{{route('admin.speciality.edit',[
                                'speciality'=>$speciality->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить эту специальность?');" href="{{route('admin.speciality.delete',[
                                'speciality'=>$speciality->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр специальности {{$speciality->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$speciality->id}}</td>
                    </tr>
                    <tr>
                        <th>НПС</th>
                        <td>{{$speciality->nps}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$speciality->name}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$speciality->description}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
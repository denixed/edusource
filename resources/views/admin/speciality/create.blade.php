@extends('layouts.admin',[
    'page_header'=>'Создать новую специальность'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новую специальность </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/speciality/form',[
                'action'=>route('admin.speciality.store'),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
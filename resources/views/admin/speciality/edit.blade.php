@extends('layouts.admin',[
    'page_header'=>'Редактировать специальность'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать специальность </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/speciality/form',[
                'action'=>route('admin.speciality.update',[
                    'speciality' => $speciality->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $speciality
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
@extends('layouts.admin',[
    'page_header'=>'Специальности'
])

@section('content')
    <div>
        <a href="{{route('admin.speciality.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить специальность</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="data-table table table-bordered table-striped">
                <col width="auto" span="4">
                <col width="150">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($specialities as $speciality)
                    <tr>
                        <th>{{$speciality->nps}}</th>
                        <td>{{$speciality->name}}</td>
                        <td>{{$speciality->description}}</td>
                        <td>
                            <a href="{{route('admin.speciality.read',[
                                'speciality'=>$speciality->id
                            ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                            <a href="{{route('admin.speciality.edit',[
                                'speciality'=>$speciality->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <a onclick="return confirm('Вы действительно хотите удалить специальность #{{$speciality->name}}?');" href="{{route('admin.speciality.delete',[
                                'speciality'=>$speciality->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
            $('.data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>
@endsection
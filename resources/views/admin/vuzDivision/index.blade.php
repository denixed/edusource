@extends('layouts.admin',[
    'page_header'=>'Подразделения'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li><a href="{{route('admin.organization.read',[
            'organization'=>$vuz->id
        ])}}">{{$vuz->organization->abbr}}</a></li>
        <li class="active">Подразделения</li>
    </ol>
@endsection
@section('content')
    <div>
        <a href="{{route('admin.vuz_division.create',[
            'vuz' => $vuz->id
        ])}}" class="btn btn-success"><i class="fa fa-plus"></i> &nbsp; Добавить подразделение</a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="data-table table table-bordered table-striped">
                <col width="auto" span="4">
                <col width="150">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($vuzDivisions as $vuzDivision)
                    @component('admin/vuzDivision/line',[
                           'vuzDivision'=>$vuzDivision
                       ])
                    @endcomponent
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')
    <!-- page script -->
    <script>
        $(function () {
            $('.data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>
@endsection
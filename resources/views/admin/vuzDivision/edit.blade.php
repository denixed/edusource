@extends('layouts.admin',[
    'page_header'=>'Редактировать кафедру'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li><a href="{{route('admin.organization.read',[
            'organization'=>$vuzDivision->vuz->id
        ])}}">{{$vuzDivision->vuz->organization->abbr}}</a></li>
        <li><a href="{{route('admin.vuz_division.list',[
            'vuz'=>$vuzDivision->vuz->id
        ])}}">Подразделения</a></li>
        <li><a href="{{route('admin.vuz_division.read',[
            'vuzDivision'=>$vuzDivision->id
        ])}}">{{$vuzDivision->name}}</a></li>
        <li class="active">Редактирование</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Редактировать кафедру </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/vuzDivision/form',[
                'action'=>route('admin.vuz_division.update',[
                    'vuzDivision' => $vuzDivision->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $vuzDivision
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
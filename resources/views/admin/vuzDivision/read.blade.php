@extends('layouts.admin',[
    'page_header'=>'Просмотр кафедры',
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li><a href="{{route('admin.organization.read',[
            'organization'=>$vuzDivision->vuz->id
        ])}}">{{$vuzDivision->vuz->organization->abbr}}</a></li>
        <li><a href="{{route('admin.vuz_division.list',[
            'vuz'=>$vuzDivision->vuz->id
        ])}}">Подразделения</a></li>
        <li class="active">{{$vuzDivision->name}}</li>
    </ol>
@endsection

@section('content')
    <div>
        <a href="{{route('admin.vuz_division.edit',[
                                'vuzDivision'=>$vuzDivision->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить эту кафедру?');" href="{{route('admin.vuz_division.delete',[
                                'vuzDivision'=>$vuzDivision->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр кафедры {{$vuzDivision->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$vuzDivision->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$vuzDivision->name}}</td>
                    </tr>
                    <tr>
                        <th>Короткое название</th>
                        <td>{{$vuzDivision->short_name}}</td>
                    </tr>
                    <tr>
                        <th>Описание</th>
                        <td>{{$vuzDivision->description}}</td>
                    </tr>
                    <tr>
                        <th>Статья</th>
                        <td>{!!$vuzDivision->about!!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
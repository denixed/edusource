<tr>
    <th>{{$vuzDivision->id}}</th>
    <td>{{$vuzDivision->short_name}}</td>
    <td>{{$vuzDivision->name}}
        @if(count($vuzDivision->children))
            <br>
            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseShowChildren-{{$vuzDivision->id}}" aria-expanded="false" aria-controls="collapseShowChildren-{{$vuzDivision->id}}">
                Показать дочерние подразделения
            </a>
        @endif
    </td>
    <td>
        <a href="{{route('admin.vuz_division.read',[
                                'vuz_division'=>$vuzDivision->id
                            ])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
        <a href="{{route('admin.vuz_division.edit',[
                                'vuz_division'=>$vuzDivision->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
        <a onclick="return confirm('Вы действительно хотите удалить подразделение #{{$vuzDivision->name}}?');" href="{{route('admin.vuz_division.delete',[
                                'vuz_division'=>$vuzDivision->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
    </td>
</tr>
@if(count($vuzDivision->children))
    <tr>

        <td colspan="4">
            <div class="collapse" id="collapseShowChildren-{{$vuzDivision->id}}">
                <table class="table-striped table table-responsive">
                    @foreach($vuzDivision->children as $vuzDivision)
                        @component('admin/vuzDivision/line',[
                               'vuzDivision'=>$vuzDivision
                           ])
                        @endcomponent
                    @endforeach
                </table>
            </div>

        </td>
    </tr>

@endif
<form method="POST" action="{{$action}}">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Название факультета</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$formData->name ?? '')  }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="short_name" class="col-md-4 col-form-label text-md-right">Короткое название факультета</label>
        <div class="col-md-6">
            <input id="short_name" type="text" class="form-control{{ $errors->has('short_name') ? ' is-invalid' : '' }}" name="short_name" value="{{ old('short_name',$formData->short_name ?? '')  }}" required autofocus>
            @if ($errors->has('short_name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('short_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">Описание факультета</label>
        <div class="col-md-6">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" required>{{ old('description',$formData->description ?? '') }}</textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="about" class="col-md-4 col-form-label text-md-right">Статья</label>
        <div class="col-md-12">
            <textarea id="about" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}" name="about">{{ old('about',$formData->about ?? '') }}</textarea>
            @if ($errors->has('about'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('about') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
<script src='/adminLTE/js/tinymce/tinymce.min.js'></script>
<script>
    var dfreeBodyConfig = {
        selector: '#about',
        menubar: false,
        height:300,
        plugins: [
            "advlist anchor autolink codesample colorpicker contextmenu fullscreen help image imagetools",
            " lists link media noneditable preview",
            " searchreplace table template textcolor visualblocks wordcount"
        ]
    };

    tinymce.init(dfreeBodyConfig);
</script>
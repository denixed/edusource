@extends('layouts.admin',[
    'page_header'=>'Создать новую кафедру'
])
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route('admin.index')}}"><i class="fa fa-dashboard"></i> Домой</a></li>
        <li><a href="{{route('admin.organization.list')}}">Организации</a></li>
        <li><a href="{{route('admin.organization.read',[
            'organization'=>$vuz->id
        ])}}">{{$vuz->organization->abbr}}</a></li>
        <li><a href="{{route('admin.vuz_division.list',[
            'vuz'=>$vuz->id
        ])}}">Подразделения</a></li>
        <li class="active">Создание</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новую кафедру </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/vuzDivision/form',[
                'action'=>route('admin.vuz_division.store',[
                    'vuz'=>$vuz->id
                ]),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
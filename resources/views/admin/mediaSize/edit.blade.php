@extends('layouts.admin',[
    'page_header'=>'Изменить размер изображений'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Изменить размер изображений </h3>
        </div>
        <!-- /.box-header -->
        <div class="alert alert-warning">
            Изменения размера не будут применены к старым изображениям!
        </div>
        <div class="box-body">
            @component('admin/mediaSize/form',[
                'action'=>route('admin.mediaSize.update',[
                    'mediaSize' => $mediaSize->id
                ]),
                'formSubmitTitle' => 'Сохранить',
                'formData' => $mediaSize
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
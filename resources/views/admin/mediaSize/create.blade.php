@extends('layouts.admin',[
    'page_header'=>'Создать новый размер изображений'
])

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Создать новый размер изображений</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @component('admin/mediaSize/form',[
                'action'=>route('admin.mediaSize.store'),
                'formSubmitTitle' => 'Создать'
            ])

            @endcomponent
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
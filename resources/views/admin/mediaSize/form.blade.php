<form method="POST" action="{{$action}}">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">Название формы обучения</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$formData->name ?? '')  }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="width" class="col-md-4 col-form-label text-md-right">Ширина</label>
        <div class="col-md-6">
            <input id="width" type="number" class="form-control{{ $errors->has('width') ? ' is-invalid' : '' }}" name="width" value="{{ old('width',$formData->width ?? '')  }}" required>
            @if ($errors->has('width'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('width') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="height" class="col-md-4 col-form-label text-md-right">Высота</label>
        <div class="col-md-6">
            <input id="height" type="number" class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}" name="height" value="{{ old('height',$formData->height ?? '')  }}" required>
            @if ($errors->has('height'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('height') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-md-4 col-form-label text-md-right">Обрезать</label>
        <div class="col-md-6">
            <select id="cropped" type="text" class="form-control{{ $errors->has('cropped') ? ' is-invalid' : '' }}" name="cropped" required>
                <option value="0" {{(old('cropped')??$formData->cropped??'')==0?'selected':''}}> Не обрезать </option>
                <option value="1" {{(old('cropped')??$formData->cropped??'')==1?'selected':''}}> Обрезать </option>
            </select>
            @if ($errors->has('cropped'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('cropped') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    
    
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ $formSubmitTitle ?? 'Отправить форму' }}
            </button>
        </div>
    </div>
</form>
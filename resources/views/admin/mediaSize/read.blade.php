@extends('layouts.admin',[
    'page_header'=>'Просмотр размера изображений'
])

@section('content')

    <div>
        <a href="{{route('admin.mediaSize.edit',[
                                'mediaSize'=>$mediaSize->id
                            ])}}" class="btn btn-primary"><i class="fa fa-edit"></i> &nbsp; Редактировать </a>
        <a onclick="return confirm('Вы действительно хотите удалить этот размер изображений?');" href="{{route('admin.mediaSize.delete',[
                                'mediaSize'=>$mediaSize->id
                            ])}}" class="btn btn-danger"><i class="fa fa-times"></i> &nbsp; Удалить </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Просмотр размера изображений {{$mediaSize->name}} </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{$mediaSize->id}}</td>
                    </tr>
                    <tr>
                        <th>Название</th>
                        <td>{{$mediaSize->name}}</td>
                    </tr>
                    <tr>
                        <th>Размер</th>
                        <td>{{$mediaSize->width}} x {{$mediaSize->height}}</td>
                    </tr>
                    <tr>
                        <th>Обрезать?</th>
                        <td>{{$mediaSize->cropped?'Да':'Нет'}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('scripts')

@endsection
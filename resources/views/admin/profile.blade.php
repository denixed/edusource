@extends('layouts.admin',[
    'page_header'=>'Профиль'
])

@section('content')
    <form method="POST" action="{{route('admin.profile.update')}}">
        @csrf

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">ФИО</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$formData->name ?? '')  }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                   Обновить
                </button>
            </div>
        </div>
    </form>
@endsection
@section('scripts')

@endsection
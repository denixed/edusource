<div class="post col-lg-4 col-md-6" data-aos="fade-up">
    <div class="post-image">
        <a href="{{route('post.readByLink',[
                        'post_url'=>$post->link
                    ])}}" title="{{$post->title}}">
            <img src="{{$post->preview_photo}}" alt="{{$post->title}}">
        </a>
    </div>
    <div>
                    <span class="teal-link">
                        {{$post->type=='новость'?'Новости':''}}
                        {{$post->type=='статья'?'Полезные статьи':''}}
                    </span>
        @foreach(explode(',',$post->tags) as $tag)
            ,<span class="teal-link">{{$tag}}</span>
        @endforeach
    </div>
    <div>
        <span class="text-muted">Опубликовано {{$post->created_at->format('d.m.Y h:i')}}</span>
    </div>
    <a href="{{route('post.readByLink',[
                    'post_url'=>$post->link
                ])}}" title="{{$post->title}}" class="title">
        {{$post->title}}
    </a>
    <div>
        {{$post->description}}
    </div>

</div>
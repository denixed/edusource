@extends('layouts.app')
@section('head')
    <title>{{$lesson->title}} на Пенале</title>

@endsection
@section('content')

    <div class="container">

        <h1>{{$lesson->title}}</h1>
        <p>Автор: {{$lesson->author}}</p>


        @if($lesson->video)
            {!! $lesson->video !!}
        @endif
        {!! $lesson->context !!}

    </div>

@endsection
@section('scripts')
    <style>
        iframe{
            width:100%;
            height:500px;
        }
    </style>
@endsection

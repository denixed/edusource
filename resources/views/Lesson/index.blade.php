@extends('layouts.app')
@section('head')
    <title>Онлайн - уроки на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mbb" style="background-image:linear-gradient(#0007,#0009),url(/img/bg-organization.png);">
        <div class="container ma">
            <h1 class="display-4 mt-4 mb-4"> Онлайн - уроки </h1>
        </div>
    </div>
    <div class="container">
        @foreach($lessonsBlocks as $lessonsBlock)
            <div class="card col-md-12">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <a href="{{route('lesson.block',[
                                'lessonsBlock'=>$lessonsBlock->id
                            ])}}">
                                @image([
                                    'media' => $lessonsBlock->image_preview
                                ])
                            </a>
                        </div>
                        <div class="col" style="display: flex; flex-direction: column;justify-content: space-between;">
                            <a href="{{route('lesson.block',[
                                'lessonsBlock'=>$lessonsBlock->id
                            ])}}">
                                <h5 class="card-title">{{$lessonsBlock->title}}</h5>
                            </a>
                            <p class="card-text">
                                <span class="btn teal-btn btn-sm"> {{$lessonsBlock->subject}}</span>
                                @if($lessonsBlock->exam)
                                    <span class="btn teal-btn btn-sm"> {{$lessonsBlock->exam}}</span>
                                @endif
                            </p>
                            <p class="card-text">{{$lessonsBlock->description}}</p>
                            <a href="{{route('lesson.block',[
                                'lessonsBlock'=>$lessonsBlock->id
                            ])}}" class="btn teal-btn d-block">Начать заниматься</a>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    </div>

@endsection
@section('scripts')

@endsection

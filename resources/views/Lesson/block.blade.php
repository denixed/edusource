@extends('layouts.app')
@section('head')
    <title>{{$lessonsBlock->title}} на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mbb" style="background-image:linear-gradient(#0007,#0009),url('{{Media::find($lessonsBlock->image_cover)->url}}');">
        <div class="container ma">
            <h1 class="display-4 mt-4 mb-4"> {{$lessonsBlock->title}} </h1>
            <p>{{$lessonsBlock->description}}</p>
        </div>
    </div>
    <div class="container">
        @foreach($lessons as $lesson)
            <div class="card col-md-12">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <a href="{{route('lesson.read',[
                                'lesson'=>$lesson->id
                            ])}}">
                                @image([
                                    'media' => $lesson->image_preview
                                ])
                            </a>
                        </div>
                        <div class="col">
                            <a href="{{route('lesson.read',[
                                'lesson'=>$lesson->id
                            ])}}">
                                <h5 class="card-title">{{$lesson->title}}</h5>
                            </a>

                            <a href="{{route('lesson.read',[
                                'lesson'=>$lesson->id
                            ])}}" class="btn teal-btn d-block">Открыть</a>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    </div>

@endsection
@section('scripts')

@endsection

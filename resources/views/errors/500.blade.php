@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center text-uppercase mt-lg-5 mb-lg-5">Произошла ошибка</h1>
        <p class="text-center mb-0">Сайт активно развивается, поэтому возможны некоторые ошибки.</p>
        <div class="text-center">
            <a href="/" class="btn btn-link">На главную</a>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center text-uppercase mt-lg-5 mb-lg-5">Доступ запрещён</h1>
        <p class="text-center mb-0">Возможно Вы попали на данную страницу по ошибке.</p>
        <p class="text-center mb-0">Проверьте, что Вы вошли в аккаунт и у Вас есть доступ к данной странице.</p>
        <div class="text-center">
            <a href="/" class="btn btn-link">На главную</a>
        </div>
    </div>

@endsection

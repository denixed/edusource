@extends('layouts.app')
@section('head')
    <title>Всё для школьников и абитуриентов на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mb-3 p-3" style="background-image:linear-gradient(rgba(0,0,0,0.7),rgba(0,0,0,0.6)),url(/img/bg.jpg);">
        <div class="container ma">
            <h1 class="display-4 mb-4" data-aos="fade-right"> Подпишись на Пенал!
            </h1>
            <h3 class="mb-3" data-aos="fade-right" data-aos-delay="300">
                <i class="fas fa-calendar-check fa-fw text-warning fa-lg mr-1"></i>
                будь в курсе всех событий в мире образования,
            </h3>
            <h3 class="mb-3" data-aos="fade-right" data-aos-delay="600">
                <i class="fas fa-video fa-fw text-warning fa-lg mr-1"></i>
                смотри разбор заданий ЕГЭ и ОГЭ преподавателями-экспертами,
            </h3>
            <h3 class="mb-4" data-aos="fade-right" data-aos-delay="900">
                <i class="fas fa-piggy-bank fa-fw text-warning fa-lg mr-1"></i>
                получай скидки от наших партнеров!
            </h3>
            <a data-aos-delay="1200" data-aos="zoom-in" href="#" class="btn btn-warning font-weight-bold col-md-3" style="color: #000;">ПОДПИСАТЬСЯ</a>
        </div>
    </div>

    @guest
    <div class="container">
        <div class="alert alert-warning">
            <b>Персональная лента отключена!</b> Авторизируйтесь на Пенале, чтобы пользоваться всеми возможностями системы.
        </div>
    </div>
    @endguest
    <div class="container ma row">
        @foreach($posts as $post)
            @include('shared.post',[
            'post'=>$post
            ])
        @endforeach
    </div>
@endsection

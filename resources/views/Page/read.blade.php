@extends('layouts.app')
@section('head')
    <title>{{$page->title}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$page->title}}. {{$page->description}}">
@endsection
@section('content')
    @component('Page/header',[
        'page'=>$page,
        'current_page'=>'about'
    ])
    @endcomponent

    <div class="container">
        {!! $page->context !!}
    </div>

@endsection

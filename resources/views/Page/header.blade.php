<div class="fullblock mb-3 p-3" style="background-image:linear-gradient(#0006,#0008),url('{{$page->cover_image??'/img/bg-olimpiada.png'}}');">
    <div class="container ma">
        <img src="{{$page->logo}}" alt="" class="mb-3" style="max-height: 150px;">
        <h1 class="display-4 align-middle">{{$page->title}}</h1>
        <p><i>{{$page->description}}</i></p>
        <a href="#" class="btn btn-warning font-weight-bold col-md-3" style="color: #000;">ПОДПИСАТЬСЯ</a>
    </div>
</div>
<div class="underline mb-4" id="tabs" style="position: sticky;
    top: 0;
    background: #fff;
    z-index: 100;">
    <div class='tag-list container ma'>
        <a class="tag{{$current_page=='about'?' active':''}}" href="{{route('page.showTab',[
            'page'=>$page->id,
            'tab'=>'about'
        ])}}#tabs">
            Описание
        </a>

        @if($page->posts()->count())
            <a class="tag{{$current_page=='posts'?' active':''}}" href="{{route('page.showTab',[
                'page'=>$page->id,
                'tab'=>'posts'
            ])}}#tabs">
                Записи и Новости
            </a>
        @endif
    </div>
</div>

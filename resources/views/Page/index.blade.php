@extends('layouts.app')
@section('head')
    <title>Страницы на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mbb" style="background-image:linear-gradient(#0007,#0009),url(/img/bg-olimpiada.jpg);">
        <div class="container ma">
            <h1 class="display-4 mt-4 mb-4"> Страницы </h1>
        </div>
    </div>
    <div class="container">
        @foreach($pages as $page)
            <div class="card col-md-6">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <a href="{{route('page.read',[
                                'page'=>$page->id
                            ])}}">
                                <img src="{{$page->logo}}" alt="">
                            </a>
                        </div>
                        <div class="col">
                            <a href="{{route('page.read',[
                                'page'=>$page->id
                            ])}}">
                                <h5 class="card-title">{{$page->title}}</h5>
                            </a>

                            <p class="card-text">{{$page->description}}</p>
                            <a href="{{route('page.read',[
                                'page'=>$page->id
                            ])}}" class="btn teal-btn d-block">Открыть</a>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    </div>

@endsection
@section('scripts')

@endsection

@extends('layouts.app')
@section('head')
    <title>Новости и записи {{$page->title}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$page->title}}. {{$page->description}}">
@endsection
@section('content')

    @component('Page/header',[
        'page'=>$page,
        'current_page'=>'posts'
    ])
    @endcomponent

    <div class="container ma row">
        @foreach(($posts=$page->posts()->paginate(10)) as $post)
            @include('shared.post',[
             'post'=>$post
             ])
        @endforeach
        @if(!count($posts))
            <div class="alert alert-warning col-12">
                К сожалению, записи не найдены.
            </div>
        @endif
    </div>
    {{$posts->links()}}
@endsection

@extends('layouts.app')
@section('head')
    <title>{{$post->title}} на Пенале</title>
    <meta description="{{$post->description}}">
    <meta keywords="{{$post->keywords}}">
@endsection
@section('content')
    <div class="fullblock mb-5" style="background-image:linear-gradient(#0007,#0009),url({{$post->preview_photo}});">
        <div class="container pt-5 pb-5">
            <div class="float-right">
                <a href="{{route('organization.read',[
                    'organization'=>$post->parent->id
                ])}}" class="h4 text-white">

                    {{$post->parent->name}}
                </a>
            </div>

            <p> {{$post->created_at}}</p>
            <h1 class="display-4">{{$post->title}}</h1>
            {{$post->description}}
        </div>
    </div>

    <section class="container ma">
        <article class="mb-4">
            <h1>
               {{$post->title}}
            </h1>
            {!! $post->context !!}
        </article>
       {{-- <div>
            <p class="h5">
                {{$post->views}} просмотров
            </p>
        </div>--}}

    </section>


@endsection

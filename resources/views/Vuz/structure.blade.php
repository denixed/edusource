@extends('layouts.app')
@section('head')
    <title>Структура ВУЗа {{$vuz->organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$vuz->organization->name}}. {{$vuz->organization->description}}">
@endsection
@section('content')

    @component('Vuz/header',[
        'vuz'=>$vuz,
        'current_page'=>'structure'
    ])
    @endcomponent

    <div class="container">
        {{--<ul>--}}
            @foreach($vuz->divisions()->whereNull('parent')->get() as $division)
                {{--<li>--}}
                    <div class="card mb-1">
                        <div class="card-body">
                            <a class="teal-link h5 card-title" href="{{route('vuz.division.read',[
                        'faculty'=>$division->id
                    ])}}">
                                <b>{{$division->name}} ({{$division->short_name}})</b>
                            </a>
                            <p class="card-text">{{$division->description}}</p>
                            {{--<a href="#" class="btn btn-primary teal-btn">Подробнее</a>--}}
                        </div>
                    </div>
                    {{--<a class="teal-link" href="{{route('vuz.division.read',[
                        'faculty'=>$division->id
                    ])}}">
                        <b>{{$division->name}} ({{$division->short_name}})</b>
                    </a>--}}
                    {{--<ul>
                        @foreach($division->children as $division)
                            <li>
                                <a class="btn teal-link" href="{{route('vuz.division.read',[
                                    'division'=>$division->id
                                ])}}"> {{$division->name}} ({{$division->short_name}}) </a>
                            </li>
                        @endforeach
                    </ul>
                </li>--}}
            @endforeach
        </ul>
    </div>



@endsection
@section('scripts')
@endsection
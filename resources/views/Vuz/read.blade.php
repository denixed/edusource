@extends('layouts.app')
@section('head')
    <title>{{$vuz->organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$vuz->organization->name}}. {{$vuz->organization->description}}">
@endsection
@section('content')

    @component('Vuz/header',[
        'vuz'=>$vuz,
        'current_page'=>'about'
    ])
    @endcomponent

    <div class="container">
        <h2>Немного о ВУЗе</h2>
        {!! $vuz->organization->about !!}
    </div>

@endsection

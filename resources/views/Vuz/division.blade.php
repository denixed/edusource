@extends('layouts.app')
@section('head')
    <title>Кафедра {{$division->name}} ВУЗа {{$division->vuz->organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$division->name}}">
@endsection
@section('content')
    <div class="fullblock mb-5" style="background-image:linear-gradient(#0007,#0009),url('/img/bg-organization.png');">
        <div class="container pt-5 pb-5">
            <p class="mb-2">Подразделение</p>
            <h1 class="mb-4">{{$division->name}}</h1>
            <p>
                <span class="mr-5">ВУЗ: </span> <a class="teal-btn btn btn-sm" href="{{route('vuz.read',[
            'vuz'=> $division->vuz->id
        ])}}">{{$division->vuz->organization->name}}</a>
            </p>
            @if($division->parent_division)
            <p>
                <span class="mr-5">Родительское подразделение: </span> <a class="teal-btn btn btn-sm" href="{{route('vuz.division.read',[
            'faculty'=> $division->parent_division->id
        ])}}">{{$division->parent_division->name}} ({{$division->parent_division->short_name}})</a>
            </p>
            @endif
            <p>
                <span class="mr-5">Короткое название подразделения:</span> {{$division->short_name}}
            </p>
        </div>
    </div>
    <div class="container mt-5">
        <p class="mb-5">{{$division->description}}</p>

        <article>
            {!! $division->about !!}
        </article>
        @if(count($prohodBalls=$division->allProhodBalls()->orderBy('year', 'desc')->get()))
            <div>
                <h2>Проходные баллы</h2>
                <table class="table table-bordered fixed-thead">
                    <thead>
                        <tr>
                            <th>
                                НПС
                            </th>
                            <th>
                                Подразделение
                            </th>
                            <th>
                                Специальность
                            </th>

                            <th>
                                Балл
                            </th>

                            <th>
                                Необходимые предметы
                            </th>
                            <th>
                                Год
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($prohodBalls as $index => $prohodBall)
                                <tr>
                                    <td>{{$prohodBall->speciality->nps}}</td>
                                    <td>
                                        <a class="teal-link" href="{{route('vuz.division.read',[
                                            'division'=>$prohodBall->division->id
                                        ])}}"> {{$prohodBall->division->name}} </a>
                                    </td>
                                    <td>{{$prohodBall->speciality->name}}</td>
                                    <td>{{$prohodBall->ball}}</td>
                                    <td>{{$prohodBall->subjects}}</td>
                                    <td>{{$prohodBall->year}}</td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @if(count($division->children))
            <div class="mb-3">
                <h2>Дочерние подразделения</h2>
                @foreach($division->children as $division)
                    <li>
                        <a class="teal-link" href="{{route('vuz.division.read',[
                                    'division'=>$division->id
                                ])}}"> Подразделение "{{$division->name}}" ({{$division->short_name}}) </a>
                    </li>
                @endforeach
            </div>
        @elseif($division->parent_division)
            <div class="mb-3">
                <h2>Другие подразделения</h2>
                @foreach($division->parent_division->children()->where('id','!=',$division->id)->get() as $division)
                    <li>
                        <a class="teal-link" href="{{route('vuz.division.read',[
                                    'division'=>$division->id
                                ])}}"> Подразделение "{{$division->name}}" ({{$division->short_name}}) </a>
                    </li>
                @endforeach
            </div>
        @else
            <div class="mb-3">
                <h2>Другие подразделения</h2>
                @foreach($division->vuz->divisions()->where('id','!=',$division->id)->whereNull('parent')->get() as $division)
                    <li>
                        <a class="teal-link" href="{{route('vuz.division.read',[
                                    'division'=>$division->id
                                ])}}"> Подразделение "{{$division->name}}" ({{$division->short_name}}) </a>
                    </li>
                @endforeach
            </div>
        @endif
    </div>

@endsection
@section('scripts')

@endsection
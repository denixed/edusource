<?php $organization=$vuz->organization;?>
<div class="container vuz">
    <div class="logo">
        <img src="{{$organization->logo_image}}" class="attachment-medium size-medium wp-post-image" alt="">	</div>
    <div class="descvuz">
        <div class="text-right float-right">
            <a href="#" class="btn teal-btn mt-3">Подписаться</a>
        </div>
        <h1 class="align-middle">{{$organization->name}}</h1>

        <p><i>{{$organization->description}}</i></p>
        <p><a class="btn-link" href="{{$vuz->site}} " target="_blank"> Сайт ВУЗа  </a></p>

        <p> <b> Рейтинг: </b>
            @if($rating=$organization->reviews()->avg('rating'))
                {{intval($rating*100)/100 }}
            @else
                Ещё нет ни одного отзыва
            @endif
        </p>
        {{--<p> <b> Город: </b> {{$vuz->city}}</p>--}}
        <p> <b> Проходной балл: </b> {{$vuz->prohod_ball()}}</p>
        <p> <b> Количество бюджетных мест: </b> {{$vuz->budgetplaces()}} </p>
        @if($vuz->hostel)
            <p> <i class="fas fa-check-square fa-lg text-success mr-1"></i> Общежитие</p>
        @endif
        @if($vuz->military)
            <p> <i class="fas fa-check-square fa-lg text-success mr-1"></i>  Военная кафедра</p>
        @endif
        {{--<p>
            <b>Специальности:</b> <br>
            <i class="fas fa-check-square fa-lg text-success mr-1"></i>
            @foreach($organization->specialities as $speciality)
                {{$speciality->name}},
            @endforeach
        </p>
        <p>
            <b>Формы обучения:</b> <br>
            @foreach($organization->eduForms as $eduForm)
                <span ><i class="fas fa-check-square fa-lg text-success mr-1"></i> {{$eduForm->name}} </span>
            @endforeach
        </p>--}}
    </div>
</div>
<div class="underline mb-4" id="tabs" style="position: sticky;
    top: 0;
    background: #fff;
    z-index: 100;">
    <div class='tag-list container ma'>
        <a class="tag{{$current_page=='about'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'about'
        ])}}#tabs">
            О ВУЗе
        </a>
        <a class="tag{{$current_page=='map'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'map'
        ])}}#tabs">
            Месторасположение
        </a>

        @if($organization->posts()->count())
            <a class="tag{{$current_page=='posts'?' active':''}}" href="{{route('vuz.showTab',[
                'vuz'=>$vuz->id,
                'tab'=>'posts'
            ])}}#tabs">
                Новости
            </a>
        @endif
        {{--<a class="tag{{$current_page=='pages'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'pages'
        ])}}#tabs">
            Страницы
        </a>
        <a class="tag{{$current_page=='events'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'events'
        ])}}#tabs">
            Мероприятия
        </a>--}}
        <a class="tag{{$current_page=='balls'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'balls'
        ])}}#tabs">
            Баллы
        </a>
        <a class="tag{{$current_page=='structure'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'structure'
        ])}}#tabs">
            Структура
        </a>
        <a class="tag{{$current_page=='reviews'?' active':''}}" href="{{route('vuz.showTab',[
            'vuz'=>$vuz->id,
            'tab'=>'reviews'
        ])}}#tabs">
            Отзывы
        </a>

    </div>
</div>

@extends('layouts.app')
@section('head')
    <title>Новости и записи ВУЗа {{$vuz->organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$vuz->organization->name}}. {{$vuz->organization->description}}">
@endsection
@section('content')

    @component('Vuz/header',[
        'vuz'=>$vuz,
        'current_page'=>'posts'
    ])
    @endcomponent

    <div class="container ma row">
        @foreach(($posts=$vuz->organization->posts()->latest()->paginate(10)) as $post)
            @include('shared.post',[
            'post'=>$post
            ])
        @endforeach
        @if(!count($posts))
            <div class="alert alert-warning col-12">
                К сожалению, у данного ВУЗа записей ещё нет.
            </div>
        @endif
    </div>
    {{$posts->links()}}
@endsection

@extends('layouts.app')
@section('head')
    <title>Отзывы о ВУЗе {{$vuz->organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$vuz->organization->name}}. {{$vuz->organization->description}}">
@endsection
@section('content')

    @component('Vuz/header',[
        'vuz'=>$vuz,
        'current_page'=>'reviews'
    ])
    @endcomponent
    <div class="container ma mb-3">
        <a class="btn teal-btn" data-toggle="collapse" href="#addReviewBlock" role="button" aria-expanded="false" aria-controls="addReviewBlock">
            Добавить свой отзыв
        </a>
    </div>
    <div class="container mb-5 collapse" id="addReviewBlock">
        @auth
            <h2>Добавить свой отзыв</h2>
            <form method="POST" action="{{route('organization.review.store',[
                'organization'=>$vuz->organization->id
            ])}}">
                @csrf

               <div class="form-group">
                    <label for="rating" class="col-md-4 col-form-label ">Ваша оценка</label>
                    <div class="col-md-6">
                        <input id="rating" type="number" min="0" max="5" step="1" class="form-control{{ $errors->has('rating') ? ' is-invalid' : '' }}" name="rating" value="{{ old('rating')  }}" required autofocus>
                        @if ($errors->has('rating'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('rating') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="ctx_plus" class="col-md-4 col-form-label ">Достоинства</label>
                    <div class="col-md-6">
                        <textarea id="ctx_plus" class="form-control{{ $errors->has('ctx_plus') ? ' is-invalid' : '' }}" name="ctx_plus" required>{{ old('ctx_plus') }}</textarea>
                        @if ($errors->has('ctx_plus'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ctx_plus') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="ctx_minus" class="col-md-4 col-form-label ">Недостатки</label>
                    <div class="col-md-6">
                        <textarea id="ctx_minus" class="form-control{{ $errors->has('ctx_minus') ? ' is-invalid' : '' }}" name="ctx_minus" required>{{ old('ctx_minus') }}</textarea>
                        @if ($errors->has('ctx_minus'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ctx_minus') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="ctx_comment" class="col-md-4 col-form-label ">Комментарий</label>
                    <div class="col-md-6">
                        <textarea id="ctx_comment" class="form-control{{ $errors->has('ctx_comment') ? ' is-invalid' : '' }}" name="ctx_comment" required>{{ old('ctx_comment') }}</textarea>
                        @if ($errors->has('ctx_comment'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ctx_comment') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mb-0">
                    <div class="col-md-6">
                        <button type="submit" class="btn teal-btn ">
                            Оправить отзыв на модерацию
                        </button>
                    </div>
                </div>
            </form>
        @else
            <div class="alert alert-warning">
                Чтобы продолжить, <a class="alert-link" href="{{route('login')}}">авторизируйтесь на Пенале</a>
            </div>
        @endauth
    </div>

    <div class="container ma">
        <h2>Отзывы</h2>
        @foreach(($reviews=$vuz->organization->reviews()->paginate(10)) as $review)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-center">{{$review->user->name}}</h5>
                    <p class="card-subtitle text-center">{{$review->created_at}}</p>
                    <div class="card-text">
                        <h6 class="bold h5 text-center">Оценка: {{$review->rating}}</h6>
                        <div class="d-flex">
                            <h6 class="bold h5 mr-3 text-right" style="min-width:150px;">Достоинства:</h6>
                            <div>
                                {{$review->ctx_plus}}
                            </div>
                        </div>
                        <div class="d-flex">
                            <h6 class="bold h5 mr-3 text-right" style="min-width:150px;">Недостатки:</h6>
                            <div>
                                {{$review->ctx_minus}}
                            </div>
                        </div>
                        <div class="d-flex">
                            <h6 class="bold h5 mr-3 text-right" style="min-width:150px;">Коментарий:</h6>
                            <div>
                                {{$review->ctx_comment}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @if(!count($reviews))
            <div class="alert alert-warning col-12">
                К сожалению, у данного ВУЗа ещё нет отзывов. Вы можете оставить первый!
            </div>
        @endif
    </div>

@endsection
@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded',function(){
            $('[data-toggle="collapse"]').click(function(e){
                e.preventDefault();
                $($(this).attr('href')).slideToggle();
                return false;
            });
        });
    </script>
@endsection
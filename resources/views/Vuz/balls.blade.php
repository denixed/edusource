@extends('layouts.app')
@section('head')
    <title>Проходные баллы ВУЗа {{$vuz->organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$vuz->organization->name}}. {{$vuz->organization->description}}">
@endsection
@section('content')

    @component('Vuz/header',[
        'vuz'=>$vuz,
        'current_page'=>'balls'
    ])
    @endcomponent
    <div class="container">
        <table class="table table-bordered table-sm table-responsive">
            <thead>
            <tr>
                <th>НПС</th>
                <th>Специальность</th>
                <th>Подразделение</th>
                <th>Бюджет</th>
                <th>Платно</th>
                <th>Необходимые предметы</th>
                <th>Год</th>
            </tr>
            </thead>
            <tbody>
            @foreach(\App\Speciality::all() as $speciality)
                @if(count($prohodBalls=$vuz->prohodBalls()->where('speciality_id',$speciality->id)->orderBy('year', 'desc')->orderBy('division_id', 'desc')->get()))

                    @foreach($prohodBalls as $index => $prohodBall)
                        <tr>
                            @if($index===0)
                                <td rowspan="{{count($prohodBalls) - $index}}">{{$speciality->nps}}</td>
                                <th rowspan="{{count($prohodBalls) - $index}}">{{$speciality->name}}</th>
                            @endif
                            <td>
                                @if($prohodBall->division)
                                <a class="teal-link" data-toggle="tooltip" href="{{route('vuz.division.read',[
                                    'division'=>$prohodBall->division->id
                                ])}}" title="{{$prohodBall->division->name}}"> {{$prohodBall->division->short_name ?? $prohodBall->division->name}} </a>
                                @endif
                            </td>
                            <td>
                                @if($prohodBall->ball)
                                    {{$prohodBall->ball}} балла <br>
                                    {{$prohodBall->budgetplaces}} мест
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                @if($prohodBall->ballpayed)
                                    {{$prohodBall->ballpayed}} балла <br>
                                    {{$prohodBall->paidplaces}} мест
                                @else
                                    -
                                @endif

                            </td>
                            <td>{{$prohodBall->subjects}}</td>
                            <td>{{$prohodBall->year}}</td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
            {{--@if(!$i)
                @foreach($prohodBalls as $index => $prohodBall)
                    <tr>
                        @if($index===0)
                            <td rowspan="{{count($prohodBalls) - $index}}">{{$speciality->nps}}</td>
                            <th rowspan="{{count($prohodBalls) - $index}}">{{$speciality->name}}</th>
                        @endif

                        <td>
                            <a class="teal-link" href="{{route('vuz.division.read',[
                                    'division'=>$prohodBall->division->vuz_faculty->id
                                ])}}"> {{$prohodBall->division->vuz_faculty->short_name}} </a>
                        </td>
                        <td>
                            <a class="teal-link" href="{{route('vuz.division.read',[
                                    'division'=>$prohodBall->division->id
                                ])}}"> {{$prohodBall->division->short_name}}. {{$prohodBall->division->name}} </a>
                        </td>
                        <td>{{$prohodBall->ball}}</td>
                        <td>{{$prohodBall->subjects}}</td>
                        <td>{{$prohodBall->year}}</td>
                    </tr>
                @endforeach
            @endif--}}
            </tbody>
        </table>
    </div>



@endsection
@section('scripts')
@endsection
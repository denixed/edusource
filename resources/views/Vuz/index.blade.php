@extends('layouts.app')
@section('head')
    <title>ВУЗы на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mbb" style="background-image:linear-gradient(#0007,#0009),url(/img/bg-vuz.jpg);">
        <div class="container ma">
            <h1 class="display-4 mt-4 mb-4" data-aos="fade-right"> ВУЗы</h1>
        </div>
    </div>
    <div class="row container ma">
        <div class="col-12">
            <p class="text-center"><b>Фильтр ВУЗов</b></p>
            <form>
                <div class="form-group">
                    <label>Поиск</label>
                    <input class="form-control" placeholder="Введите ваш запрос" name="s" value="{{\Request::get('s')}}">
                </div>
                <div class="form-group">
                    <input id="check_hostel" type="checkbox" name="hostel" {{\Request::get('hostel')=='on'?'checked':''}}>
                    <label for="check_hostel">Общежитие</label>
                </div>
                <div class="form-group">
                    <input id="check_military" type="checkbox" name="military" {{\Request::get('military')=='on'?'checked':''}}>
                    <label for="check_military">Военная кафедра</label>
                </div>

                <div class="form-group">
                    <label>Специальность</label>
                    <select class="form-control" name="specialities">
                        <option value="">Не выбрано</option>
                        @foreach(\App\Speciality::whereNull('parent')->get() as $speciality)
                            <option value="{{$speciality->id}}" {{\Request::get('specialities')==$speciality->id ?'selected':''}}>{{$speciality->name}} ({{$speciality->nps}})</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group">
                    <label>Регион</label>
                    <select class="form-control" name="region">
                        <option value="">Не выбрано</option>
                        @foreach(\App\Organization::groupBy('region')->select('region',DB::raw(' COUNT(*) AS `count`'))->orderByDesc('count')->get() as $region)
                            <option value="{{$region->region}}" {{\Request::get('region')!=""&&\Request::get('region')==$region->region ?'selected':''}}>{{$region->region}}</option>
                        @endforeach

                    </select>
                </div>
                <button type="submit" class="btn teal-btn d-block" name="filter" value="on" style="width: 100%;">Подобрать</button>
            </form>
        </div>
    </div>
    <div class="container" >
        <table class="table table-hover text-center table-sm align-middle table-bordered table-responsive" id="results">
            <colgroup width="75">
            </colgroup><thead>
            <tr>
                <th colspan="2">ВУЗ</th>
                <th>Проход<wbr>ной балл</th>
                <th>Бюджет<wbr>ных мест</th>
                <th>Обще<wbr>житие </th>
                <th>Военная кафедра</th>
                <th>Регион</th>
            </tr>
            </thead>
            <tbody>
            @foreach($vuzs as $vuz)
                <?php
                $organization=$vuz;
                ?>
                <tr id="post-49">
                    <td style="border-right:none;" @if($organization->ball) rowspan="2" @endif>
                        <img src="{{$organization->logo_image}}" alt="">
                    </td>
                    <td class="text-left align-middle" style="border-left:none;">
                        <a href="{{route('vuz.read',[
                                'vuz' => $vuz->id
                            ])}}" title="Подробнее о {{$organization->abbr}}" style="text-decoration:none;">
                            <p>
                                <b>
                                    {{$organization->abbr}}
                                </b>
                                <br>
                                {{$organization->name}}
                            </p>
                        </a>
                    </td>
                    {{--<td>{{$vuz->city}}</td>--}}
                    <td>{{$vuz->prohod_ball()}}</td>
                    <td>{{$vuz->budgetplaces()}}</td>

                    <td @if($organization->ball) rowspan="2" @endif>
                        @if($vuz->hostel)
                            <i class="fas fa-check-circle fa-2x text-success"></i>
                        @endif
                    </td>
                    <td @if($organization->ball) rowspan="2" @endif>
                        @if($vuz->military)
                            <i class="fas fa-check-circle fa-2x text-success"></i>
                        @endif
                    </td>
                    <td @if($organization->ball) rowspan="2" @endif>
                        {{$organization->region}}
                    </td>
                </tr>
                @if($organization->ball)
                    <tr>
                        <td class="text-left align-middle">
                            <b>{{$organization->speciality_name}}</b>
                            <a href="{{route('vuz.showTab',[
                                'vuz'=>$vuz->id,
                                'tab'=>'balls'
                            ])}}#tabs" class="teal-link">Показать ещё</a>
                        </td>
                        <td>
                            {{$organization->ball}}
                        </td>
                        <td>
                            {{$organization->budgetplaces}}
                        </td>
                    </tr>
                @endif
            @endforeach


            </tbody>
        </table>
    </div>
    {{$vuzs->appends([
            's' => \Request::get('s'),
            'military' => \Request::get('military'),
            'hostel' => \Request::get('hostel'),
            'filter' => 'on',
            'specialities' => \Request::get('specialities'),
            'region' => \Request::get('region')
            ])->links()}}

@endsection
@section('scripts')

    <script>

        document.addEventListener('DOMContentLoaded',function(){
            var filter = {{\Request::get('filter')?'true':'false'}};
            if(filter){
                window.scrollTo(0,document.querySelector('table').offsetTop-100)
            }
        });

    </script>
@endsection

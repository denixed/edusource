@extends('layouts.app')
@section('head')
    <title> Месторасположение ВУЗа{{$vuz->organization->abbr}} на Пенале</title>
    <meta description="На Пенали Вы можете познакомиться с {{$vuz->organization->name}}. {{$vuz->organization->description}}">
@endsection
@section('content')

    @component('Vuz/header',[
        'vuz'=>$vuz,
        'current_page'=>'map'
    ])
    @endcomponent

    <div class="container">
        <h2>Месторасположение</h2>
        <p>
            <b>Адрес:</b> {{$vuz->organization->address}}
        </p>

        <div id="map"></div>
        <style>#map{width:100%;height:400px;}</style>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script>
            ymaps.ready(init);
            function init() {
                var myMap = new ymaps.Map('map', {
                    center: [{{$vuz->organization->cord}}].reverse(),
                    zoom: 16,
                    controls: ['zoomControl','typeSelector',  'fullscreenControl']

                });
                var myPlacemark = new ymaps.Placemark([{{$vuz->organization->cord}}].reverse(), {
                    iconContent: '{{$vuz->organization->abbr}}',
                    balloonContent: ''
                }, {
                    preset: 'islands#violetStretchyIcon'
                });
                myMap.geoObjects.add(myPlacemark);
            };
        </script>
    </div>

@endsection

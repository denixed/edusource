<!doctype html>
<html lang="ru-ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


@yield('head')
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta name="description" content="">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#00897B">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#00897B">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#00897B">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">

</head>
<body data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
<nav class='top-nav underline'>
    <a class='btn teal-btn d-block d-md-none text-white' id='showmenu'> <b> {{--&#8801;--}} Пенал</b>.онлайн <i class="fas fa-caret-down"></i></a>
    <ul class="menu d-none d-md-flex">
        <li class="d-none d-md-block"><a href="{{route('feed')}}" style="padding:5px">
                <img style="height:50px;padding:2px 2px 1px;" src="/img/logo.svg" alt="">
            </a>
        </li>
        <li class="d-block d-md-none">
            <a href="{{route('feed')}}">Лента</a>
        </li>

        <li><a href="{{route('vuz.index')}}">ВУЗы</a></li>
        <li><a href="{{route('page.index')}}">Олимпиады</a></li>
        <li><a href="{{route('kurs.index')}}">Курсы</a></li>
        <li><a href="{{route('lesson.index')}}">Онлайн-уроки</a></li>
        <li><a href="{{route('testing.index')}}">Тесты ЕГЭ и ОГЭ</a>
           {{-- <ul class="sub-menu">
                @foreach(\App\Exam::all() as $exam)
                    <li><a href="#{{$exam->id}}">{{$exam->name}}</a></li>
                @endforeach
            </ul>--}}
        </li>
        @guest
            <li><a href="{{route('login')}}">Войти</a></li>
        @else
            <li>
                <a href="#"> {{Auth::user()->name}} </a>
                <ul class="sub-menu" style="right: 0;">
                    @if(Auth::user()->isAdmin()||Auth::user()->isAuthor())
                        <a href="{{route('admin.index')}}">Админ панель</a>
                    @endif
                    <a href='{{ route('logout') }}' onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Выйти</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </li>
        @endguest
    </ul>
</nav>
<main>
    @yield('content')
</main>
<footer class="footer">
    <div class="container ma">
        <form method="post" class="text-center">
            <h3>Узнайте о новостях образования первым!</h3>
            <input class='form-control' type="text" placeholder="Email" type='email'>
            <button class="btn teal-btn">Подписаться</button>
            <p> ✅ Я даю своё согласие на обработку моих персональных данных</p>
        </form>
    </div>
    <div class="container ma row">
        <div class="col-md-6  mbb">
            <div class="secondary-white mb">Ближайший вебинар</div>
            <a href='#'>ЕГЭ по математике. Сдаем без проблем</a>
            <p class="secondary-white">7 апреля 2018 г. в 16:00 по Москве</p>
            <div>
                <img src="img/tutor1.jpg" width="30" class='rounded-circle' style='margin-right: 10px;'> Валенита Александровна
            </div>
        </div>
        <div class="col-md-6  mbb">
            <div class="secondary-white mb">Последние записи</div>
            <?php /*
                $args = array(
                    'numberposts' => 3,
                    'post_status' => 'publish',
                );

                $result = wp_get_recent_posts($args);

                foreach( $result as $p ){
                ?>
                <div>
                    <a href="<?php echo get_permalink($p['ID']) ?>"><?php echo $p['post_title'] ?></a>
                </div>
                <?php
                }*/
            ?>
        </div>
    </div>
    <div class="ma container row justify-content-between">
        <div class="col-md-auto mbb">
            <div class="secondary-white mb">penal.online</div>
        </div>
        <div class="col-md-auto mbb">
            <p class="mb">info@penal.online</p>
            <a href='#'>Реквизиты</a>
        </div>
    </div>
    <div class="ma container row justify-content-between mb">
        <div class="col-md-auto">
            © 2018-2019 Penal.online. Все права защищены
        </div>
        <div class="col-md-auto">
            <a href='#'> Политика в отношении обработки персональных данных </a>
        </div>
    </div>
    <div>&nbsp;</div>
</footer>
<script src="/js/jquery.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        $('[sort-tg]').click(function () {
            var id = parseInt($(this).attr('sort-tg'));
            $(this).parent().children('.active').removeClass('active');
            $(this).addClass('active');
            if (!id) {
                $('.post').show(500);
            } else {
                $('.post').addClass('notActive');
                $('.post.category-' + id).removeClass('notActive').addClass('Active');
                $('.post.notActive').hide(200);
                $('.post.Active').show(200);
                $('.post').removeClass('notActive').removeClass('Active');
            }
        });
        $('#showmenu').click(function () {
            $('.menu').toggleClass('d-none d-block');
        });
    });
</script>
@yield('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
    AOS.init({
        // Global settings:
        disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
        startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
        initClassName: 'aos-init', // class applied after initialization
        animatedClassName: 'aos-animate', // class applied on animation
        useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
        disableMutationObserver: false, // disables automatic mutations' detections (advanced)
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
        throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        offset: 20, // offset (in px) from the original trigger point
        delay: 0, // values from 0 to 3000, with step 50ms
        duration: 400, // values from 0 to 3000, with step 50ms
        easing: 'ease', // default easing for AOS animations
        once: true, // whether animation should happen only once - while scrolling down
        mirror: false, // whether elements should animate out while scrolling past them
        anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

    });
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

</body>
</html>

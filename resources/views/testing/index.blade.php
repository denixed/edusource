@extends('layouts.app')
@section('head')
    <title>Тесты ЕГЭ и ОГЭ на Пенале</title>

@endsection
@section('content')
    <div class="container">
        <ul class="nav nav-pills mt-3 mb-3" id="exams" role="tablist">
            @foreach($exams as $exam)
                <li class="nav-item">
                    <a class="nav-link" id="exam-tab-{{$exam->id}}" data-toggle="tab" href="#exam-{{$exam->id}}" role="tab" aria-controls="exam-{{$exam->id}}" aria-selected="true">{{$exam->name}}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content" id="examsChoice">
            @foreach($exams as $exam)
                <div class="tab-pane row" id="exam-{{$exam->id}}" role="tabpanel" aria-labelledby="exam-tab-{{$exam->id}}">
                    @foreach($exam->subjects as $subject)
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($subject->logo)
                                        <div class="col-4">
                                            <a href="#">
                                                <img src="{{$subject->logo}}" alt="">
                                            </a>
                                        </div>
                                    @endif
                                    <div class="col">
                                        <a href="{{route('testing.subject',[
                                            'subject'=>$subject
                                        ])}}">
                                            <h5 class="card-title">{{$subject->name}}</h5>
                                        </a>
                                        <a href="{{route('testing.subject',[
                                            'subject'=>$subject
                                        ])}}" class="btn teal-btn d-block">Начать заниматься</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>

@endsection
@section('scripts')

@endsection

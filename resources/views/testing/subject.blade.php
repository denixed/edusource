@extends('layouts.app')
@section('head')
    <title>Тесты ЕГЭ и ОГЭ на Пенале</title>

@endsection
@section('content')
    <div class="container">
        <div>
            @if($subject->logo)
                <div class="col-4">
                    <a href="#">
                        <img src="{{$subject->logo}}" alt="">
                    </a>
                </div>
            @endif
            <div class="col">
                <h1 class="display-4 text-center">{{$subject->name}}</h1>
            </div>
        </div>
        <h2>Структура</h2>
        <div>
            <table class="table table-bordered table-striped">
                <tbody>
                @foreach($subject->themes as $theme)
                    <tr>
                        <th>{{$theme->number}}</th>
                        <td>
                            <a href="#" class="">{{$theme->name}}</a>
                            <a class='btn btn-link teal-link' data-toggle="collapse" href="#collapse-prototype-{{$theme->id}}" role="button" aria-expanded="false" aria-controls="collapse-prototype-{{$theme->id}}">Показать прототипы</a>
                            <div class="collapse" id="collapse-prototype-{{$theme->id}}">
                                <ul class="list-group">
                                    @foreach($theme->prots as $prot)
                                        <a href="#" class="list-group-item  list-group-item-action">{{$prot->name}}</a>
                                    @endforeach
                                </ul>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
@section('scripts')

@endsection

@extends('layouts.app')
@section('head')
    <title>Новости и записи {{$organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$organization->name}}. {{$organization->description}}">
@endsection
@section('content')

    @component('Organization/header',[
        'organization'=>$organization,
        'current_page'=>'posts'
    ])
    @endcomponent

    <div class="container ma row">
        @foreach(($posts=$organization->posts()->paginate(10)) as $post)
            @include('shared.post',[
             'post'=>$post
             ])
        @endforeach
        @if(!count($posts))
            <div class="alert alert-warning col-12">
                К сожалению, у данной организации записей ещё нет.
            </div>
        @endif
    </div>
    {{$posts->links()}}
@endsection

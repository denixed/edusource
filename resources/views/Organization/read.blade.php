@extends('layouts.app')
@section('head')
    <title>{{$organization->name}} на Пенале</title>
    <meta description="На Пенале Вы можете познакомиться с {{$organization->name}}. {{$organization->description}}">
@endsection
@section('content')

    @component('Organization/header',[
        'organization'=>$organization,
        'current_page'=>'about'
    ])
    @endcomponent

    <div class="container">
        <h2>Немного о данной организации</h2>
        {!! $organization->about !!}
    </div>

@endsection

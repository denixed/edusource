@extends('layouts.app')
@section('head')
    <title>ВУЗы на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mbb" style="background-image:linear-gradient(#0007,#0009),url(/img/bg-vuz.jpg);">
        <div class="container ma">
            <h1 class="display-4 mt-4 mb-4"> ВУЗы</h1>
        </div>
    </div>
    <div class="row ma">
        <div class="col-lg-2 filter">
            <p class="text-center"><b>Фильтр ВУЗов</b></p>
            <form>
                <div class="form-group">
                    <label>Проходной балл</label>
                    <input type="number" class="form-control" placeholder="Введите ваши баллы ЕГЭ" name="ball" value="{{\Request::get('ball')}}">
                </div>
                <div class="form-group">
                    <label>Город</label>
                    <select class="form-control" name="city">
                        <option value="">Любой</option>
                        <option>Москва</option>
                        <option>Санкт-Петербург</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Специальность</label>
                    <select class="form-control" name="specialties">
                        <option value="">Любая</option>
                        @foreach(App\Speciality::all() as $speciality)
                            <option value="{{$speciality->id}}">{{$speciality->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input id="check_hostel" type="checkbox" name="hostel" {{\Request::get('hostel')=='on'?'checked':''}}>
                    <label for="check_hostel">Общежитие</label>
                </div>
                <div class="form-group">
                    <input id="check_military" type="checkbox" name="military" {{\Request::get('military')=='on'?'checked':''}}>
                    <label for="check_military">Военная кафедра</label>
                </div>
                <button type="submit" class="btn teal-btn d-block" name="filter" value="on" style="width: 100%;">Подобрать</button>
            </form>
        </div>
        <div class="col-lg-10 responsive-table">
            <table class="table table-hover table-sm text-center align-middle">
                <colgroup width="150">
                </colgroup><thead>
                <tr>
                    <td colspan="2">ВУЗ</td>
                    <td>Город</td>
                    <td>Проходной&nbsp;балл</td>
                    <td>Бюджетных&nbsp;мест</td>
                    <td>Общежитие </td>
                    <td>Военная&nbsp;кафедра</td>
                </tr>
                </thead>
                <tbody>
                @foreach($vuzs as $vuz)
                    <?php
                        $organization=$vuz->organization;
                    ?>
                    <tr id="post-49">
                        <td>
                            <img src="{{$organization->logo_image}}" alt="">
                        </td>
                        <td class="text-left">
                            <a href="{{route('vuz.read',[
                                'vuz' => $vuz->id
                            ])}}" title="{{$organization->name}}">
                                <h3>{{$organization->name}}</h3>
                                <p>{{$organization->description}}</p>
                            </a>
                        </td>
                        <td>{{$vuz->city}}</td>
                        <td>{{$vuz->min_ball}}</td>
                        <td>{{$vuz->budget}}</td>

                        <td>
                            @if($vuz->hostel)
                                <i class="fas fa-check-circle fa-2x text-success"></i>
                            @endif
                        </td>
                        <td>
                            @if($vuz->military)
                                <i class="fas fa-check-circle fa-2x text-success"></i>
                            @endif
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('scripts')

    <script>
        document.addEventListener('DOMContentLoaded',function(){
            $('.filter select[name="city"]').val('{{\Request::get('city')}}');
            $('.filter select[name="specialties"]').val('{{\Request::get('specialties')}}');
        });
    </script>
@endsection

<div class="fullblock mb-3 p-3" style="background-image:linear-gradient(#0006,#0008),url('/img/bg-organization.png');">
    <div class="container ma">
        <img src="{{$organization->logo_image}}" alt="" class="mb-3" style="max-height: 150px;">
        <h1 class="display-4 align-middle">{{$organization->name}}</h1>
        <p><i>{{$organization->description}}</i></p>
        <p>
            <i class="fas fa-smile fa-fw text-warning fa-lg mr-1"></i>
            <b> Рейтинг: </b>
            @if($rating=$organization->reviews()->avg('rating'))
                {{intval($rating*100)/100 }}
            @else
                Ещё нет ни одного отзыва
            @endif
        </p>
        <a href="#" class="btn btn-warning font-weight-bold col-md-3" style="color: #000;">ПОДПИСАТЬСЯ</a>
        <a href="{{route('organization.showTab',[
            'organization'=>$organization->id,
            'tab'=>'reviews'
        ])}}#tabs" class="btn btn-warning font-weight-bold col-md-3" style="color: #000;">ОСТАВИТЬ ОТЗЫВ</a>
    </div>
</div>
<div class="underline mb-4" id="tabs" style="position: sticky;
    top: 0;
    background: #fff;
    z-index: 100;">
    <div class='tag-list container ma'>
        <a class="tag{{$current_page=='about'?' active':''}}" href="{{route('organization.showTab',[
            'organization'=>$organization->id,
            'tab'=>'about'
        ])}}#tabs">
            Об организации
        </a>
        <a class="tag{{$current_page=='map'?' active':''}}" href="{{route('organization.showTab',[
            'organization'=>$organization->id,
            'tab'=>'map'
        ])}}#tabs">
            Месторасположение
        </a>
        @if($organization->posts()->count())
            <a class="tag{{$current_page=='posts'?' active':''}}" href="{{route('organization.showTab',[
                'organization'=>$organization->id,
                'tab'=>'posts'
            ])}}#tabs">
                Записи и Новости
            </a>
        @endif
        {{--<a class="tag{{$current_page=='pages'?' active':''}}" href="{{route('organization.showTab',[
            'organization'=>$organization->id,
            'tab'=>'pages'
        ])}}#tabs">
            Страницы
        </a>
        <a class="tag{{$current_page=='events'?' active':''}}" href="{{route('organization.showTab',[
            'organization'=>$organization->id,
            'tab'=>'events'
        ])}}#tabs">
            Мероприятия
        </a>--}}
        <a class="tag{{$current_page=='reviews'?' active':''}}" href="{{route('organization.showTab',[
            'organization'=>$organization->id,
            'tab'=>'reviews'
        ])}}#tabs">
            Отзывы об организации
        </a>

    </div>
</div>

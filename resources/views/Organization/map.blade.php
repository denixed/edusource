@extends('layouts.app')
@section('head')
    <title> Месторасположение ВУЗа{{$organization->name}} на Пенале</title>
    <meta description="На Пенали Вы можете познакомиться с {{$organization->name}}. {{$organization->description}}">
@endsection
@section('content')

    @component('Organization/header',[
        'organization'=>$organization,
        'current_page'=>'map'
    ])
    @endcomponent

    <div class="container">
        <h2>Месторасположение</h2>
        <p>
            <b>Адрес:</b> {{$organization->address}}
        </p>

        <div id="map"></div>
        <style>#map{width:100%;height:400px;}</style>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script>
            ymaps.ready(init);
            function init() {
                var myMap = new ymaps.Map('map', {
                    center: [{{$organization->cord}}].reverse(),
                    zoom: 16,
                    controls: ['zoomControl','typeSelector',  'fullscreenControl']

                });
                var myPlacemark = new ymaps.Placemark([{{$organization->cord}}].reverse(), {
                    iconContent: '{{$organization->name}}',
                    balloonContent: ''
                }, {
                    preset: 'islands#violetStretchyIcon'
                });
                myMap.geoObjects.add(myPlacemark);
            };
        </script>
    </div>

@endsection

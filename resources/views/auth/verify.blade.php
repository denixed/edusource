@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Подтвердите свой аккаунт</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Новая ссылка подтверждения аккаунта была отправлена на ваш email адрес.
                        </div>
                    @endif

                    Перед тем, как продолжить, необходимо подтвердить email адрес.
                    Если вы не получили письмо, <a class="teal-link" href="{{ route('verification.resend') }}"> нажмите сюда , и мы вышлем Вам письмо повторно</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

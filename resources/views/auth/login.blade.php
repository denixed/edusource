@extends('layouts.app')
@section('head')
    <title>Войти в Пенал</title>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 mt-lg-5 mb-lg-3">
            <h1 class="h3 text-center">Войти</h1>
        </div>
        <div class="col-md-8 mb-lg-5">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf

                <div class="form-group row">
                    <label for="email" class="col-sm-4 col-form-label text-md-right">E-mail</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" checked>

                            <label class="form-check-label" for="remember">
                                Запомнить меня
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn teal-btn">
                           Войти
                        </button>
                        &nbsp;
                        <a class="btn teal-link" href="{{ route('register') }}">
                            Регистрация
                        </a>
                        <a class="btn teal-link" href="{{ route('password.request') }}">
                            Забыли пароль?
                        </a>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

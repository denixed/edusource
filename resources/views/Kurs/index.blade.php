@extends('layouts.app')
@section('head')
    <title>ВУЗы на Пенале</title>

@endsection
@section('content')
    <div class="fullblock mbb" style="background-image:linear-gradient(#0007,#0009),url(/img/bg-organization.png);">
        <div class="container ma">
            <h1 class="display-4 mt-4 mb-4"> Подготовительные курсы</h1>
        </div>
    </div>
    <div class="container">
        @foreach($organizations as $organization)
            <div class="card col-md-6">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <a href="{{route('organization.read',[
                                'organization'=>$organization->id
                            ])}}">
                                <img src="{{$organization->logo_image}}" alt="">
                            </a>
                        </div>
                        <div class="col">
                            <a href="{{route('organization.read',[
                                'organization'=>$organization->id
                            ])}}">
                                <h5 class="card-title">{{$organization->name}}</h5>
                            </a>

                            <p class="card-text">{{$organization->description}}</p>
                            <a href="{{route('organization.read',[
                                'organization'=>$organization->id
                            ])}}" class="btn teal-btn d-block">Открыть</a>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    </div>

@endsection
@section('scripts')

@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use FullTextSearch;
    protected $fillable = [
        'title', 'description', 'seo_keywords', 'link',
        'tags', 'type', 'context', 'preview_photo',
        'organization_id', 'event_id', 'page_id',
        'show_in_feed','status',
        'views', 'id'
    ];
    protected $searchable=[
        'title', 'description', 'seo_keywords', 'tags', 'context'
    ];
    public function parent(){
        //dd($this->belongsTo('App\Organization'));
        if($this->event_id) return $this->belongsTo('App\Event','event_id','id','event');
        else if($this->page_id) return $this->belongsTo('App\Page','page_id','id','page');
        else if($this->organization_id) return $this->belongsTo('App\Organization','organization_id','id','organization');
        else return null;
    }
}

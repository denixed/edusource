<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class VuzDivision extends Model
{
    protected $fillable=[
        'name','short_name','description','about','vuz_id'
    ];


    public function vuz(){
        return $this->belongsTo('App\Vuz');
    }
    public function children ()
    {
        return $this->hasMany(VuzDivision::class,'parent' );
    }

    public function parent_division ()
    {
        return $this->belongsTo(VuzDivision::class, 'parent');
    }

    public function getAllChildren ()
    {
        $divisions = new Collection();

        foreach ($this->children as $division) {
            $divisions->push($division);
            $divisions = $divisions->merge($division->getAllChildren());
        }

        return $divisions;
    }
    public function prohodBalls(){
        return $this->hasMany('App\ProhodBall','division_id');
    }
    public function allProhodBalls(){
        return ProhodBall::whereIn('division_id', $this->getAllChildren()->push($this)->pluck('id'));
    }
}

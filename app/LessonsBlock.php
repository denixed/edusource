<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonsBlock extends Model
{
    protected $fillable = [
        'title', 'authors', 'description', 'image_cover', 'image_preview', 'subject', 'exam', 'views', 'lessons_block_id'
    ];


    protected $casts = [
        'options' => 'array',
    ];

    function lessons(){
        return $this->hasMany(Lesson::class);
    }
}


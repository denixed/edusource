<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $fillable=[
        'name','description', 'nps','parent'
    ];

    public function organizations(){
        return $this->belongsToMany('App\Organization');
    }
    public function children(){
        return $this->hasMany('App\Speciality','parent');
    }
}

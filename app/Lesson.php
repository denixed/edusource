<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable=[
        'title','context', 'video', 'type', 'author', 'image_preview', 'views', 'lessons_block_id'
    ];

    function lessonsBlock (){
        return $this->belongsTo(LessonsBlock::class);
    }
}

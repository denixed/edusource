<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "url"=>$this->url,
            "parent"=>$this->parent,
            "type"=>$this->type,
            "title"=>$this->title,
            "height"=>$this->height,
            "width"=>$this->width,
            "children"=>MediaResource::collection($this->children),
            "media_size_id"=>$this->mediaSize?[
                "id"=>$this->mediaSize->id,
                "name"=>$this->mediaSize->name,
                "height"=>$this->mediaSize->height,
                "width"=>$this->mediaSize->width,
                "cropped"=>$this->mediaSize->cropped,
            ]:null
        ];
    }
}

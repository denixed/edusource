<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\LessonsBlock;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Lesson/index',[
            'lessonsBlocks' => LessonsBlock::all()
        ]);
    }

    public function block(LessonsBlock $lessonsBlock)
    {
        return view('Lesson/block',[
            'lessonsBlock' => $lessonsBlock,
            'lessons' => $lessonsBlock->lessons
        ]);
    }
    public function read(Lesson $lesson)
    {
        return view('Lesson/lesson',[
            'lesson' => $lesson
        ]);
    }
}

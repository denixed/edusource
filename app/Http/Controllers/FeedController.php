<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    public function index(){
        return view('feed',[
            'posts'=>Post::where('status','=',1)->where('show_in_feed','=',1)->latest()->paginate(10)
        ]);
    }
    public function news(){
        return view('feed',[
            'posts'=>Post::where('status','=',1)->where('show_in_feed','=',1)->where('type','новость')->latest()->paginate(10)
        ]);
    }
    public function articles(){
        return view('feed',[
            'posts'=>Post::where('status','=',1)->where('show_in_feed','=',1)->where('type','статья')->latest()->paginate(10)
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function index(Request $request)
    {
        $organizations=Organization::where('status', '=', 1);
        return view('Organization/index',[
            'organizations'=>$organizations->orderByDesc('id')->paginate(10)
        ]);

    }
    public function indexKurs(Request $request)
    {
        $organizations=Organization::where('status', '=', 1)->where('type','LIKE','курсы');
        return view('Kurs/index',[
            'organizations'=>$organizations->orderByDesc('id')->paginate(10)
        ]);

    }

    public function show(Organization $organization)
    {
        if($organization->vuz) return redirect(route('vuz.read',[
            'vuz'=>$organization->vuz->id
        ]));
        $organization->increment('views');

        return view('Organization/read',[
            'organization'=>$organization
        ]);
    }
    public function showTab(Organization $organization, $tab)
    {
        if($organization->vuz) return redirect(route('vuz.read',[
            'vuz'=>$organization->vuz->id
        ]));
        if($tab=='about') return view('Organization/read',[
            'organization'=>$organization
        ]);
        else if ($tab=='map') return view('Organization/map',[
            'organization'=>$organization
        ]);
        else if ($tab=='posts') return view('Organization/posts',[
            'organization'=>$organization
        ]);
        else if ($tab=='reviews') return view('Organization/reviews',[
            'organization'=>$organization
        ]);
        abort(404);
    }
}

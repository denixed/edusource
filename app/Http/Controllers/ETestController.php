<?php

namespace App\Http\Controllers;

use App\ETest;
use Illuminate\Http\Request;

class ETestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ETest  $eTest
     * @return \Illuminate\Http\Response
     */
    public function show(ETest $eTest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ETest  $eTest
     * @return \Illuminate\Http\Response
     */
    public function edit(ETest $eTest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ETest  $eTest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ETest $eTest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ETest  $eTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ETest $eTest)
    {
        //
    }
}

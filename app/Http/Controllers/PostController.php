<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post->increment('views');

        return view('post',[
            'post'=>$post
        ]);
    }

    public function showByLink($post_url)
    {
        if(!($post=Post::where('link','like',$post_url)->first())) abort(404);
        $post->increment('views');
        return view('post',[
            'post'=>$post
        ]);
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends \App\Http\Controllers\Controller{

    protected  function postCheckParent($parent_type,$parent_id){
        if(!in_array($parent_type,['Organization','Page'])) abort(404);
        $parentClass='App\\'.$parent_type;
        if(!($parent=$parentClass::find($parent_id)))  abort(404);
        return $parent;
    }

    public function index($parent_type,$parent_id,Request $request)
    {
        $parent=$this->postCheckParent($parent_type,$parent_id);
        if($request->has('s')&&$request->get('s')!=''){
            $data= $parent->posts()->search($request->get('s'))->paginate(10);
        }else{
            $data=$parent->posts()->paginate(10);
        }
        return view('admin/post/index',[
            'posts'=>$data,
            'parent_type'=>$parent_type,
            'parent_id'=>$parent_id,
            'parent'=>$parent
        ]);
    }
    public function create($parent_type,$parent_id)
    {
        $this->postCheckParent($parent_type,$parent_id);
        return view('admin/post/create',[
            'parent_type'=>$parent_type,
            'parent_id'=>$parent_id
        ]);
    }

    public function store($parent_type,$parent_id,Request $request)
    {
        $parent=$this->postCheckParent($parent_type,$parent_id);
        $request->validate([
            'preview_photo'=>'image',
        ]);

        $post=$parent->posts()->create($request->validate([
            'title'=>'required|string|max:255',
            'link'=>'required|string|unique:posts',
            'description'=>'required|string|max:255',
            'seo_keywords'=>'required|string|max:255',
            'tags'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'context'=>'required|string',
            'show_in_feed'=>'integer|min:0|max:1'
        ]));
        if($request->file('preview_photo')){
            $request->file('preview_photo')->storeAs(
                'public/posts/'.$post->id, 'preview'
            );
            $post->preview_photo= '/storage/posts/'.$post->id.'/preview';
            $post->save();
        }
        return redirect(route('admin.post.edit',[
            'post'=>$post->id
        ]));
    }

    public function read(Post $post)
    {
        return view('admin/post/read',[
            'post'=>$post
        ]);
    }
    public function edit(Post $post)
    {
        return view('admin/post/edit',[
            'post'=>$post
        ]);
    }

    public function update(Request $request, Post $post)
    {
        $post->update($request->validate([
            'status'=>'integer|min:0|max:1',
            'title'=>'required|string|max:255',
            'link'=>'required|string',
            'description'=>'required|string|max:255',
            'seo_keywords'=>'required|string|max:255',
            'tags'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'context'=>'required|string',
            'show_in_feed'=>'integer|min:0|max:1',
        ]));
        if($request->file('preview_photo')){
            $request->file('preview_photo')->storeAs(
                'public/posts/'.$post->id, 'preview'
            );
            $post->preview_photo= '/storage/posts/'.$post->id.'/preview';
            $post->save();
        }
        return redirect(route('admin.post.read',[
            'post'=>$post->id
        ]));
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect(route('admin.post.list'));
    }
}
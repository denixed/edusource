<?php
namespace App\Http\Controllers\Admin;

use App\Vuz;
use App\VuzDivision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VuzDivisionController extends \App\Http\Controllers\Controller{

    public function index(Vuz $vuz)
    {
        return view('admin/vuzDivision/index',[
            'vuzDivisions'=>$vuz->divisions()->whereNull('parent')->get(),
            'vuz'=>$vuz
        ]);
    }
    public function create(Vuz $vuz)
    {
        return view('admin/vuzDivision/create',[
            'vuz'=>$vuz
        ]);

    }

    public function store(Request $request,Vuz $vuz)
    {
        $vuzDivision=$vuz->divisions()->create($request->validate([
            'name'=>'required|string|max:255',
            'short_name'=>'required|string|max:255',
            'description'=>'required|string|max:255',
            'about'=>'required|string'
        ]));
        return redirect(route('admin.vuz_division.read',[
            'vuzDivision'=>$vuzDivision->id
        ]));
    }
    public function edit(VuzDivision $vuzDivision)
    {
        return view('admin/vuzDivision/edit',[
            'vuzDivision'=>$vuzDivision,
        ]);
    }
    public function read(VuzDivision $vuzDivision)
    {
        return view('admin/vuzDivision/read',[
            'vuzDivision'=>$vuzDivision,
        ]);
    }

    public function update(Request $request, VuzDivision $vuzDivision)
    {
        $vuzDivision->update($request->validate([
            'name'=>'required|string|max:255',
            'short_name'=>'required|string|max:255',
            'description'=>'required|string|max:255',
            'about'=>'required|string'
        ]));
        return redirect(route('admin.vuz_division.read',[
            'vuzDivision'=>$vuzDivision->id
        ]));
    }

    public function destroy(VuzDivision $vuzDivision)
    {
        $vuz = $vuzDivision->vuz;
        $vuzDivision->delete();
        return redirect(route('admin.vuz_division.list',[
            'vuz'=>$vuz->id
        ]));
    }
}
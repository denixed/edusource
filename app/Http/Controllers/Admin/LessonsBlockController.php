<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\LessonsBlock;
use Illuminate\Http\Request;

class LessonsBlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.lessonsBlock.index',[
            'lessonsBlocks'=>LessonsBlock::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.lessonsBlock.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $lessonsBlock = LessonsBlock::create($request->validate([
            'title'=>'required|string|max:255',
            //'authors'=>'required|array',
            //'authors.*.name'=>'required|string|max:255',
            //'authors.*.*'=>'string|max:255',
            'description'=>'required|string|max:255',
            'image_cover'=>'required|string|max:255',
            'image_preview'=>'required|string|max:255',
            'subject'=>'nullable|string|max:255',
            'exam'=>'required|string|max:255',
        ]));

        return redirect(route('admin.lessons.block.read',[
            'lessonsBlock' => $lessonsBlock
        ]));
    }



    public function show(LessonsBlock $lessonsBlock)
    {
        return view('admin.lessonsBlock.read',[
            'lessonsBlock' => $lessonsBlock
        ]);
    }

    public function edit(LessonsBlock $lessonsBlock)
    {
        return view('admin.lessonsBlock.edit',[
            'lessonsBlock' => $lessonsBlock
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, LessonsBlock $lessonsBlock)
    {
        $lessonsBlock->update($request->validate([
            'title'=>'required|string|max:255',
            //'authors'=>'required|array',
            //'authors.*.name'=>'required|string|max:255',
            //'authors.*.*'=>'string|max:255',
            'description'=>'required|string|max:255',
            'image_cover'=>'required|string|max:255',
            'image_preview'=>'required|string|max:255',
            'subject'=>'string|max:255|nullable',
            'exam'=>'required|string|max:255',
        ]));

        return redirect(route('admin.lessons.block.read',[
            'lessonsBlock' => $lessonsBlock
        ]));
    }


    public function destroy(LessonsBlock $lessonsBlock)
    {
        $lessonsBlock->delete();
        return redirect(route('admin.lessons.block.list'));
    }
}

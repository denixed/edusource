<?php
namespace App\Http\Controllers\Admin;

use App\Http\Resources\MediaResource;
use App\Media;
use App\MediaSize;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class MediaController extends \App\Http\Controllers\Controller{

    public function index(Request $request)
    {
        return view('admin/media/index');
    }
    public function list(Request $request)
    {
        return MediaResource::collection(Media::whereNull('parent')->orderByDesc('created_at','desc')->paginate(10));
    }
    public function store(Request $request)
    {
        $data=$request->validate([
            'title'=>'required|string|max:255',
            'file'=>'required|file|mimes:jpeg,bmp,png'
        ]);


        $img_original = Image::make($request->file('file'));

        $name= date('Y-m-d_h:i:s');

        $media_parent = Media::create([
            'title'=>$data['title'],
            'url'=> '/storage/uploads/'.$name.'.png',
            'width'=>$img_original->width(),
            'height'=>$img_original->height(),
            'type'=>1
        ]);
        $img_original->save(storage_path('app/public/uploads/'.$name.'.png'));
        $aspect = $img_original->width() > $img_original->height();
        foreach(MediaSize::all() as $size){
            $img = Image::make($request->file('file'));
            if($size->cropped){
                $sizedImage = $img->fit($size->width,$size->height);
            }else{
                if($aspect){
                    $sizedImage = $img->widen($size->width);

                }else{
                    $sizedImage = $img->heighten($size->height);
                }
            }
            $sizedImage->save(storage_path('app/public/uploads/'.$name.'.s'.$size->id.'.png'));

            Media::create([
                'title'=>$data['title'],
                'url'=> '/storage/uploads/'.$name.'.s'.$size->id.'.png',
                'width'=>$sizedImage->width(),
                'height'=>$sizedImage->height(),
                'type'=>1,
                'media_size_id'=>$size->id,
                'parent'=>$media_parent->id
            ]);
        }

        return new MediaResource($media_parent);
    }
    public function update(Request $request, Media $media)
    {

        $media->update($request->validate([
            'title'=>'required|string|max:255',
        ]));
        return response()->json(new MediaResource($media));
    }

    public function destroy(Media $media)
    {
        $media->delete();
        return response()->json('ok');
    }


}
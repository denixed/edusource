<?php
namespace App\Http\Controllers\Admin;

use App\Organization;
use App\ProhodBall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrganizationController extends \App\Http\Controllers\Controller{

    public function index(Request $request)
    {
        $data=null;

        if($request->has('s')&&$request->get('s')!=''){
            $data= Organization::search($request->get('s'))->paginate(10);
        }else{
            $data=Organization::paginate(10);
        }
        return view('admin/organization/index',[
            'organizations'=>$data
        ]);
    }
    public function create()
    {
        return view('admin/organization/create');
    }

    public function store(Request $request)
    {
        $organization=Organization::create($request->validate([
            'status'=>'required|integer|max:2',
            'name'=>'required|string|max:255',
            'abbr'=>'required|string|max:255',
            'description'=>'required',
            'contact_full_name'=>'required|string|max:255',
            'contact_number'=>'required|string|max:63',
            'logo_image'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'additional'=>'required|string|max:255',
            'about'=>'',
            'address'=>'string|max:255',
            'cord' => 'string|max:255',
        ]));
        if($organization->type=='вуз') Vuz::create([
            'id'=>$organization->id
        ]);
        return redirect(route('admin.organization.edit',[
            '$organization'=>$organization->id
        ]));
    }
    public function edit(Organization $organization)
    {
        return view('admin/organization/edit',[
            'organization'=>$organization
        ]);
    }
    public function read(Organization $organization)
    {
        return view('admin/organization/read',[
            'organization'=>$organization
        ]);
    }

    public function update(Request $request, Organization $organization)
    {

        $data=$request->validate([
            'status'=>'required|integer|max:2',
            'name'=>'required|string|max:255',
            'abbr'=>'required|string|max:255',
            'description'=>'required',
            'contact_full_name'=>'required|string|max:255',
            'contact_number'=>'required|string|max:63',
            'logo_image'=>'string|max:255',
            'type'=>'required|string|max:255',
            'additional'=>'required|string|max:255',
            'about'=>'',
            'address'=>'string|max:255',
            'cord' => 'string|max:255',
        ]);
        $request->validate([
            'logo'=>'image',
        ]);
        if($request->file('logo')){
            $request->file('logo')->storeAs(
                'public/organizations', $organization->id
            );
            $data['logo_image']= '/storage/organizations/'.$organization->id;
        }

        if($vuz=$organization->vuz) $vuz->update($request->validate([
            'city'=>'required|string|max:255',
            'min_ball'=>'required|integer',
            'budget'=>'required|integer',
            'site'=>'required|string|max:255',
            'hostel'=>'required|integer|max:1|min:0',
            'military'=>'required|integer|max:1|min:0',
        ]));
        $organization->update($data);
        if($specialities=$request->post('specialities')){
            $organization->specialities()->detach();
            foreach(array_keys($specialities) as $id) $organization->specialities()->attach($id);
        }
        if($eduForms=$request->post('eduForms')){
            $organization->eduForms()->detach();
            foreach(array_keys($eduForms) as $id) $organization->eduForms()->attach($id);
        }
        return redirect(route('admin.organization.read',[
            '$organization'=>$organization->id
        ]));
    }

    public function destroy(Organization $organization)
    {
        if($vuz=$organization->vuz()) $vuz->delete();
        $organization->delete();
        return redirect(route('admin.organization.list'));
    }

    public function vuzEditProhodBalls(Organization $organization)
    {
        return view('admin/organization/vuz/editProhodBalls',[
            'organization'=>$organization
        ]);
    }
    public function vuzUpdateProhodBalls(Organization $organization, Request $request)
    {
        $data=$request->all(['newprohodBall','prohodBall']);
        if($data['newprohodBall']) foreach( $data['newprohodBall'] as $newprohodBall){
            $organization->vuz->prohodBalls()->create($newprohodBall);
        }
        if($data['prohodBall']) foreach( $data['prohodBall'] as $id => $prohodBall){
            ProhodBall::find($id)->update($prohodBall);
        }
        return redirect(route('admin.organization.vuz.editProhodBalls',[
            'organization'=>$organization->id
        ]));
    }

}
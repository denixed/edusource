<?php
namespace App\Http\Controllers\Admin;

use App\Vuz;
use App\VuzFaculty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VuzFacultyController extends \App\Http\Controllers\Controller{

    public function index(Vuz $vuz)
    {
        return view('admin/vuzFaculty/index',[
            'vuzFaculties'=>$vuz->faculties,
            'vuz'=>$vuz
        ]);
    }
    public function create(Vuz $vuz)
    {
        return view('admin/vuzFaculty/create',[
            'vuz'=>$vuz
        ]);

    }

    public function store(Request $request,Vuz $vuz)
    {
        $vuzFaculty=$vuz->faculties()->create($request->validate([
            'name'=>'required|string|max:255',
            'short_name'=>'required|string|max:255',
            'description'=>'required|string|max:255',
            'about'=>'required|string'
        ]));
        return redirect(route('admin.vuz_faculty.read',[
            'vuzFaculty'=>$vuzFaculty->id
        ]));
    }
    public function edit(VuzFaculty $vuzFaculty)
    {
        return view('admin/vuzFaculty/edit',[
            'vuzFaculty'=>$vuzFaculty,
            'vuz'=>$vuzFaculty->vuz
        ]);
    }
    public function read(VuzFaculty $vuzFaculty)
    {
        return view('admin/vuzFaculty/read',[
            'vuzFaculty'=>$vuzFaculty,
            'vuz'=>$vuzFaculty->vuz
        ]);
    }

    public function update(Request $request, VuzFaculty $vuzFaculty)
    {
        $vuzFaculty->update($request->validate([
            'name'=>'required|string|max:255',
            'short_name'=>'required|string|max:255',
            'description'=>'required|string|max:255',
            'about'=>'required|string'
        ]));
        return redirect(route('admin.vuz_faculty.read',[
            'vuzFaculty'=>$vuzFaculty->id
        ]));
    }

    public function destroy(VuzFaculty $vuzFaculty)
    {
        $vuzFaculty->delete();
        return redirect(route('admin.vuz_faculty.list'));
    }
}
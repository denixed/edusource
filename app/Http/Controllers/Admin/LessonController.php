<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lesson;
use App\LessonsBlock;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LessonsBlock $lessonsBlock)
    {
        return view('admin/lesson/index',[
            'lessons'=>$lessonsBlock->lessons()->paginate(),
            'lessonsBlock' => $lessonsBlock
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(LessonsBlock $lessonsBlock)
    {
        return view('admin/lesson/create',[
            'lessonsBlock'=>$lessonsBlock
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, LessonsBlock $lessonsBlock)
    {
        $lesson = $lessonsBlock->lessons()->create($request->validate([
            'title'=>'required|string|max:255',
            'context'=>'required|string',
            'video'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'author'=>'required|string|max:255',
            'image_preview'=>'required|integer',

        ]));
        return redirect(route('admin.lesson.read',[
            'lesson'=>$lesson
        ]));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        return view('admin/lesson/read',[
            'lesson' => $lesson
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        return view('admin/lesson/edit',[
            'lesson' => $lesson
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesson $lesson)
    {
        $lesson->update($request->validate([
            'context'=>'required|string',
            'video'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'author'=>'required|string|max:255',
            'image_preview'=>'required|integer',

        ]));
        return redirect(route('admin.lesson.read',[
            'lesson'=>$lesson
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        $lessonsBlock = $lesson->lessonsBlock;
        $lesson->delete();
        return redirect(route('admin.lesson.index',[
            'lessonsBlock'=>$lessonsBlock
        ]));
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\MediaSize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaSizeController extends \App\Http\Controllers\Controller{

    public function index(Request $request)
    {
        return view('admin/mediaSize/index',[
            'mediaSizes'=>MediaSize::all()
        ]);
    }
    public function create()
    {
        return view('admin/mediaSize/create');
    }

    public function store(Request $request)
    {
        $mediaSize=MediaSize::create($request->validate([
            'name'=>'required|string|max:255',
            'width'=>'required|integer|min:0|max:4096',
            'height'=>'required|integer|min:0|max:4096',
            'cropped'=>'required|integer|min:0|max:1',
        ]));
        return redirect(route('admin.mediaSize.read',[
            'mediaSize'=>$mediaSize->id
        ]));
    }
    public function edit(MediaSize $mediaSize)
    {
        return view('admin/mediaSize/edit',[
            'mediaSize'=>$mediaSize
        ]);
    }
    public function read(MediaSize $mediaSize)
    {
        return view('admin/mediaSize/read',[
            'mediaSize'=>$mediaSize
        ]);
    }

    public function update(Request $request, MediaSize $mediaSize)
    {

        $mediaSize->update($request->validate([
            'name'=>'required|string|max:255',
            'width'=>'required|integer|min:0|max:4096',
            'height'=>'required|integer|min:0|max:4096',
            'cropped'=>'required|integer|min:0|max:1',
        ]));
        return redirect(route('admin.mediaSize.read',[
            'mediaSize'=>$mediaSize->id
        ]));
    }

    public function destroy(MediaSize $mediaSize)
    {
        $mediaSize->delete();
        return redirect(route('admin.mediaSize.list'));
    }
    

}
<?php

namespace App\Http\Controllers;

use App\EduForm;
use App\EProt;
use App\ESubject;
use App\ETheme;
use App\Exam;
use App\Organization;
use App\Page;
use App\Post;
use App\ProhodBall;
use App\Speciality;
use App\Vuz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function index()
    {
        return view('admin/index');
    }

    public function specialityIndex()
    {
        return view('admin/speciality/index',[
            'specialities'=>Speciality::all()
        ]);
    }
    public function specialityCreate()
    {
        return view('admin/speciality/create');
    }

    public function specialityStore(Request $request)
    {
        $speciality=Speciality::create($request->validate([
            'name'=>'required|string|max:255',
            'description'=>'required|string|max:1023',
            'nps'=>'required|string|max:20|unique:specialities'
        ]));
        return redirect(route('admin.speciality.read',[
            'speciality'=>$speciality->id
        ]));
    }
    public function specialityEdit(Speciality $speciality)
    {
        return view('admin/speciality/edit',[
            'speciality'=>$speciality
        ]);
    }
    public function specialityRead(Speciality $speciality)
    {
        return view('admin/speciality/read',[
            'speciality'=>$speciality
        ]);
    }

    public function specialityUpdate(Request $request, Speciality $speciality)
    {
        $speciality->update($request->validate([
            'name'=>'required|string|max:255',
            'description'=>'required|string|max:1023',
            'nps'=>'required|string|max:20'
        ]));
        return redirect(route('admin.speciality.read',[
            'speciality'=>$speciality->id
        ]));
    }

    public function specialityDestroy(Speciality $speciality)
    {
        $speciality->delete();
        return redirect(route('admin.speciality.list'));
    }

    public function profileIndex()
    {
        return view('admin/profile',[
            'formData'=>Auth::user()
        ]);
    }

    public function profileUpdate(Request $request)
    {
        Auth::user()->update($request->validate([
            'name'=>'required|string|max:255',
        ]));
        return redirect(route('admin.profile.index'));
    }

    public function eduFormIndex()
    {
        return view('admin/eduForm/index',[
            'eduForms'=>EduForm::all()
        ]);
    }
    public function eduFormCreate()
    {
        return view('admin/eduForm/create');
    }

    public function eduFormStore(Request $request)
    {
        $eduForm=EduForm::create($request->validate([
            'name'=>'required|string|max:255',
            'description'=>'required|string|max:1023'
        ]));
        return redirect(route('admin.eduForm.read',[
            'eduForm'=>$eduForm->id
        ]));
    }
    public function eduFormEdit(EduForm $eduForm)
    {
        return view('admin/eduForm/edit',[
            'eduForm'=>$eduForm
        ]);
    }
    public function eduFormRead(EduForm $eduForm)
    {
        return view('admin/eduForm/read',[
            'eduForm'=>$eduForm
        ]);
    }

    public function eduFormUpdate(Request $request, EduForm $eduForm)
    {
        $eduForm->update($request->validate([
            'name'=>'required|string|max:255',
            'description'=>'required|string|max:1023'
        ]));
        return redirect(route('admin.eduForm.read',[
            'eduForm'=>$eduForm->id
        ]));
    }

    public function eduFormDestroy(EduForm $eduForm)
    {
        $eduForm->delete();
        return redirect(route('admin.eduForm.list'));
    }
    // Exams

    public function examIndex()
    {
        return view('admin/exam/index',[
            'exams'=>Exam::all()
        ]);
    }
    public function examCreate()
    {
        return view('admin/exam/create');
    }

    public function examStore(Request $request)
    {
        $exam=Exam::create($request->validate([
            'name'=>'required|string|max:255',
            'content'=>'required|string|max:1023'
        ]));
        return redirect(route('admin.exam.read',[
            'exam'=>$exam->id
        ]));
    }
    public function examEdit(Exam $exam)
    {
        return view('admin/exam/edit',[
            'exam'=>$exam
        ]);
    }
    public function examRead(Exam $exam)
    {
        return view('admin/exam/read',[
            'exam'=>$exam
        ]);
    }

    public function examUpdate(Request $request, Exam $exam)
    {
        $exam->update($request->validate([
            'name'=>'required|string|max:255',
            'content'=>'required|string|max:1023'
        ]));
        return redirect(route('admin.exam.read',[
            'exam'=>$exam->id
        ]));
    }

    public function examDestroy(Exam $exam)
    {
        $exam->delete();
        return redirect(route('admin.exam.list'));
    }
    //Subjects

    public function subjectIndex(Exam $exam)
    {
        return view('admin/subject/index',[
            'subjects'=>$exam->subjects,
            'exam'=>$exam
        ]);
    }
    public function subjectCreate(Exam $exam)
    {
        return view('admin/subject/create',[
            'exam'=>$exam
        ]);

    }

    public function subjectStore(Request $request,Exam $exam)
    {
        $subject=$exam->subjects()->create($request->validate([
            'name'=>'required|string|max:255',
        ]));
        return redirect(route('admin.subject.read',[
            'subject'=>$subject->id
        ]));
    }
    public function subjectEdit(ESubject $subject)
    {
        return view('admin/subject/edit',[
            'subject'=>$subject
        ]);
    }
    public function subjectRead(ESubject $subject)
    {
        return view('admin/subject/read',[
            'subject'=>$subject
        ]);
    }

    public function subjectUpdate(Request $request, ESubject $subject)
    {
        $subject->update($request->validate([
            'name'=>'required|string|max:255',
        ]));
        return redirect(route('admin.subject.read',[
            'subject'=>$subject->id
        ]));
    }

    public function subjectDestroy(ESubject $subject)
    {
        $subject->delete();
        return redirect(route('admin.subject.list'));
    }
    //Themes

    public function themeIndex(ESubject $subject)
    {
        return view('admin/theme/index',[
            'themes'=>$subject->themes,
            'subject'=>$subject
        ]);
    }
    public function themeCreate(ESubject $subject)
    {
        return view('admin/theme/create',[
            'subject'=>$subject
        ]);

    }

    public function themeStore(Request $request,ESubject $subject)
    {
        $theme=$subject->themes()->create($request->validate([
            'name'=>'required|string|max:255',
            'number'=>'required|string|max:255',
            'in_exam'=>'required|integer|min:0|max:1',
            'content'=>'string'
        ]));
        return redirect(route('admin.theme.read',[
            'theme'=>$theme->id
        ]));
    }
    public function themeEdit(ETheme $theme)
    {
        return view('admin/theme/edit',[
            'theme'=>$theme
        ]);
    }
    public function themeRead(ETheme $theme)
    {
        return view('admin/theme/read',[
            'theme'=>$theme
        ]);
    }

    public function themeUpdate(Request $request, ETheme $theme)
    {
        $theme->update($request->validate([
            'name'=>'required|string|max:255',
            'number'=>'required|string|max:255',
            'in_exam'=>'required|integer|min:0|max:1',
            'content'=>'string'
        ]));
        return redirect(route('admin.theme.read',[
            'theme'=>$theme->id
        ]));
    }

    public function themeDestroy(ETheme $theme)
    {
        $theme->delete();
        return redirect(route('admin.theme.list'));
    }
    // Prototypes

    public function protCreate(ETheme $theme)
    {
        return view('admin/prot/create',[
            'theme'=>$theme
        ]);

    }

    public function protStore(Request $request,ETheme $theme)
    {
        $prot=$theme->prots()->create($request->validate([
            'name'=>'required|string|max:255',
            'content'=>''
        ]));
        return redirect(route('admin.prot.read',[
            'prot'=>$prot->id
        ]));
    }
    public function protEdit(EProt $prot)
    {
        return view('admin/prot/edit',[
            'prot'=>$prot
        ]);
    }
    public function protRead(EProt $prot)
    {
        return view('admin/prot/read',[
            'prot'=>$prot
        ]);
    }

    public function protUpdate(Request $request, EProt $prot)
    {
        $prot->update($request->validate([
            'name'=>'required|string|max:255',
            'content'=>''
        ]));
        return redirect(route('admin.prot.read',[
            'prot'=>$prot->id
        ]));
    }

    public function protDestroy(EProt $prot)
    {
        $prot->delete();
        return redirect(route('admin.prot.list'));
    }
    
    // Pages

    public function pageIndex(Request $request)
    {
        if($request->has('s')&&$request->get('s')!=''){
            $data= Page::search($request->get('s'))->paginate(10);
        }else{
            $data=Page::paginate(10);
        }
        return view('admin/page/index',[
            'pages'=>$data
        ]);
    }
    public function pageCreate()
    {
        return view('admin/page/create');
    }

    public function pageStore(Request $request)
    {
        $request->validate([
            'preview_photo'=>'image',
        ]);

        $page=Page::create($request->validate([
            'title'=>'required|string|max:255',
            'description'=>'required|string|max:255',
            'seo_keywords'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'context'=>'required|string',
        ]));
        if($request->file('logo')){
            $request->file('logo')->storeAs(
                'public/pages/'.$page->id, 'logo'
            );
            $page->logo= '/storage/pages/'.$page->id.'/logo';
            $page->save();
        }
        if($request->file('cover_image')){
            $request->file('cover_image')->storeAs(
                'public/pages/'.$page->id, 'cover'
            );
            $page->cover_image= '/storage/pages/'.$page->id.'/cover';
            $page->save();
        }
        return redirect(route('admin.page.read',[
            'page'=>$page->id
        ]));
    }

    public function pageRead(Page $page)
    {
        return view('admin/page/read',[
            'page'=>$page
        ]);
    }
    public function pageEdit(Page $page)
    {
        return view('admin/page/edit',[
            'page'=>$page
        ]);
    }

    public function pageUpdate(Request $request, Page $page)
    {
        $page->update($request->validate([
            'status'=>'integer|min:0|max:1',
            'title'=>'required|string|max:255',
            'description'=>'required|string|max:255',
            'seo_keywords'=>'required|string|max:255',
            'type'=>'required|string|max:255',
            'context'=>'required|string',
        ]));
        if($request->file('logo')){
            $request->file('logo')->storeAs(
                'public/pages/'.$page->id, 'logo'
            );
            $page->logo= '/storage/pages/'.$page->id.'/logo';
            $page->save();
        }
        if($request->file('cover_image')){
            $request->file('cover_image')->storeAs(
                'public/pages/'.$page->id, 'cover'
            );
            $page->cover_image= '/storage/pages/'.$page->id.'/cover';
            $page->save();
        }
        return redirect(route('admin.page.read',[
            'page'=>$page->id
        ]));
    }

    public function pageDestroy(Page $page)
    {
        $page->delete();
        return redirect(route('admin.page.list'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $pages=Page::where('status', '=', 1);
        return view('Page/index',[
            'pages'=>$pages->orderByDesc('id')->paginate(10)
        ]);

    }

    public function show(Page $page)
    {
        $page->increment('views');
        return view('Page/read',[
            'page'=>$page
        ]);
    }
    public function showTab(Page $page, $tab)
    {
        if($tab=='about') return view('Page/read',[
            'page'=>$page
        ]);
        else if ($tab=='posts') return view('Page/posts',[
            'page'=>$page
        ]);
        abort(404);
    }
}

<?php

namespace App\Http\Controllers;

use App\ProhodBall;
use App\Speciality;
use App\Vuz;
use App\VuzDivision;
use App\VuzFaculty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VuzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vuzs=Vuz::join('organizations', 'vuzs.id', '=', 'organizations.id')->where('status', '=', 1);
        if(\Request::get('s')) $vuzs->whereHas('organization', function ($query) {
            $query->search(\Request::get('s'));
        });

        if($request -> get ('filter')=='on'){
            if($ball=$request->get('ball')){
                $vuzs=$vuzs->where('min_ball','<=',$ball+10);
            }
            if($city=$request->get('city')){
                $vuzs=$vuzs->where('city','LIKE',$city);
            }
            if($specialities=$request->get('specialties')){
                $vuzs=$vuzs->whereHas('organization.specialities', function ($query) use($specialities) {
                    $query->where('speciality_id', '=', $specialities);
                });
            }
            if($hostel=$request->get('hostel')=='on'){
                $vuzs=$vuzs->where('hostel','=',1);
            }
            if($military=$request->get('military')=='on'){
                $vuzs=$vuzs->where('military','=',1);
            }

            if($region=$request->get('region')){
                $vuzs=$vuzs->where('region','LIKE',$region);
            }
        }
        if(!\Request::get('specialities')){
            $vuzs = $vuzs
                /*->join( 'reviews', 'reviews.organization_id', '=', 'vuzs.id' )
                ->groupBy( 'vuzs.id' )
                ->select( 'vuzs.id', DB::raw( 'AVG( reviews.rating )' ) )*/
                ->orderByDesc('views');
        }else{
            $speciality = $request->get('specialities');

            $speciality = Speciality::find($speciality);
            if(!$speciality) return abort(404);
            $ch = $speciality->children()->select('id')->get();
            $specialities =[];
            array_push($specialities,$speciality->id);
            foreach($ch as $i){
                array_push($specialities,$i->id);
            }

            $vuzs = $vuzs->where('status', '=', 1)
                ->join('prohod_balls', 'vuzs.id', '=', 'prohod_balls.vuz_id')
                ->whereIn('speciality_id',$specialities)
                ->whereNotNull('ball')
                ->orderByDesc('views')
                ->orderBy('ball')
                ->groupBy('vuz_id')
                ->join('specialities', 'prohod_balls.speciality_id', '=', 'specialities.id')
                ->select('prohod_balls.*','specialities.*','vuzs.*','organizations.*','specialities.name as speciality_name','specialities.description as speciality_description');

            //dd($vuzs->limit(10)->get()[0]);
        }

        return view('Vuz/index',[
           'vuzs'=>$vuzs->orderByDesc('logo_image')->paginate(20)
        ]);
    }


    public function show(Vuz $vuz)
    {
        $vuz->organization()->increment('views');
        return view('Vuz/read',[
            'vuz'=>$vuz
        ]);
    }
    public function showTab(Vuz $vuz, $tab)
    {
        if($tab=='about') return view('Vuz/read',[
            'vuz'=>$vuz
        ]);
        else if ($tab=='map') return view('Vuz/map',[
            'vuz'=>$vuz
        ]);
        else if ($tab=='posts') return view('Vuz/posts',[
            'vuz'=>$vuz
        ]);
        else if ($tab=='reviews') return view('Vuz/reviews',[
            'vuz'=>$vuz
        ]);
        else if ($tab=='balls') return view('Vuz/balls',[
            'vuz'=>$vuz
        ]);
        else if ($tab=='structure') return view('Vuz/structure',[
            'vuz'=>$vuz
        ]);
        abort(404);
    }
    public function showFaculty(VuzFaculty $faculty)
    {
        return view('Vuz/faculty',[
            'faculty'=>$faculty
        ]);
    }
    public function showDivision(VuzDivision $division)
    {
        //dd($division->vuz_faculty);
        return view('Vuz/division',[
            'division'=>$division
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use App\ESubject;
use App\Exam;
use Illuminate\Http\Request;

class TestingController extends Controller
{
    public function index(){
        return view('testing.index',[
            'exams' => Exam::all()
        ]);
    }
    public function subject(ESubject $subject){
        return view('testing.subject',[
            'subject' => $subject
        ]);
    }
}

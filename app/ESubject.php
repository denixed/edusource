<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ESubject extends Model
{
    protected $fillable=[
        'name','icon','e_exam_id'
    ];
    public function exam(){
        return $this->belongsTo('App\Exam','e_exam_id','id','exam');
    }
    public function themes(){
        return $this->hasMany('App\ETheme','e_subject_id','id');
    }
}

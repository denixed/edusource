<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vuz extends Model
{
    protected $fillable=[
      'id','city','address','min_ball','budget',
      'site','hostel','military'
    ];

    public function organization(){
        return $this->hasOne('App\Organization', 'id');
    }

    public function prohodBalls(){
        return $this->hasMany('App\ProhodBall');
    }
    public function prohod_ball(){
        return intval($this->prohodBalls()->where('ball','>',0)->avg('ball'));
    }
    public function budgetplaces(){
        return $this->prohodBalls()->sum('budgetplaces');
    }
    public function divisions(){
        return $this->hasMany('App\VuzDivision','vuz_id');
    }

}

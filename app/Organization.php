<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'name','description','contact_full_name',
        'contact_number','logo_image','type','additional',
        'status','about','address','cord',
        'views','abbr'
    ];
    protected $searchable = [
        'name',
        'description',
        'about',
        'abbr'
    ];
    use FullTextSearch;

    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function rating()
    {
        if(!$this->reviews()->count()) return null;
        return intval($this->reviews()->avg('rate')*100)/100;
    }

    public function pages(){
        return $this->belongsToMany('App\Page');
    }

    public function events(){
        return $this->belongsToMany('App\Event');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
    public function vuz(){
        return $this->hasOne('App\Vuz', 'id');
    }
    public function specialities(){
        return $this->belongsToMany('App\Speciality');
    }
    public function eduForms(){
        return $this->belongsToMany('App\EduForm');
    }
    public function reviews(){
        return $this->hasMany('App\Review');
    }
    public function hasSpeciality($id){
        return $this->specialities->find($id);
    }
    public function hasEduForm($id){
        return $this->eduForms->find($id);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ETheme extends Model
{
    //
    protected $fillable=[
        'number','name','content','in_exam','e_subject_id'
    ];
    public function subject(){
        return $this->belongsTo('App\ESubject','e_subject_id','id','e_subject');
    }
    public function prots(){
        return $this->hasMany('App\EProt','e_theme_id','id');
    }

}

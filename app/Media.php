<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $fillable=[
        'id','url','parent','type','title','height','width','media_size_id'
    ];

    public function mediaSize(){
        return $this->belongsTo('App\MediaSize');
    }
    public function children(){
        return $this->hasMany(Media::class, 'parent');
    }
}

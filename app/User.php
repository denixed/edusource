<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','city','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function organizations(){
        return $this->belongsToMany('App\Organizations');
    }
    public function roles(){
        return $this->belongsToMany('App\Role');
    }
    public function forms(){
        return $this->belongsToMany('App\EduForm');
    }
    public function isAdmin(){
        return $this->roles->find(99)==true;
    }
    public function isAuthor(){
        return $this->roles->find(91)==true;
    }
    public function reviews(){
        return $this->hasMany('App\Review');
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new Notifications\EmailVerificationNotification);
    }
}

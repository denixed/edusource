<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaSize extends Model
{
    //
    protected $fillable=[
        'id','name','height','width','cropped'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable=[
        'rating','ctx_plus','ctx_minus','ctx_comment','user_id','organization_id',
        'status'
    ];
    public function organization(){
        return $this -> belongsTo('App\Organization');
    }
    public function user(){
        return $this -> belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable=[
      'name', 'content'
    ];
    public function subjects(){
        return $this->hasMany('App\ESubject','e_exam_id','id');
    }
}

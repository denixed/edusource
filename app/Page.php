<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'description', 'seo_keywords',
        'logo', 'cover_image', 'context', 'parent',
        'type','views','status'
    ];
    public function parent(){
        return $this->hasOne('App\Post','id','parent');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function organizations(){
        return $this->belongsToMany('App\Organization');
    }

}

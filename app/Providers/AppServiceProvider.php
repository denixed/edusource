<?php

namespace App\Providers;

use App\Media;
use function foo\func;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Resource::withoutWrapping();

        Blade::directive('image', function ($data) {
            return '<?php
                        $data = '.$data.';
                        $media = $data["media"];
                        $attrs = $data["attrs"]??"";
                        $media = Media::find($media); 
                        if($media):
                            $sizes = $media->children->map(function($media){
                                return $media->url." ".$media->width."w";
                            })->implode(", ");
                          
                    ?><img src="<?= $media->url ?>" srcset="<?= $sizes ?>" alt="<?= $media->title ?>" <?= $attrs ?>> <?php endif; ?>';
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

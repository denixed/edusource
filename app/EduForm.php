<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EduForm extends Model
{
    protected $fillable=[
        'name','description'
    ];

    public function organizations(){
        return $this->belongsToMany('App\Organization');
    }
}

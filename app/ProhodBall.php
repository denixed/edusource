<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProhodBall extends Model
{
    public $fillable = [
        'vuz_id','year','speciality_id','division_id','subjects','ball','ballpayed','budgetplaces','paidplaces'
    ];

    public function vuz(){
        return $this->hasOne('App\Vuz');
    }
    public function speciality(){
        return $this->belongsTo('App\Speciality');
    }
    public function division(){
        return $this->belongsTo('App\VuzDivision');
    }

}

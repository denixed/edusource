<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EProt extends Model
{
    protected $fillable=[
        'e_theme_id','name','content'
    ];
    public function theme (){
        return $this->belongsTo('App\ETheme','e_theme_id','id','e_theme');
    }
}

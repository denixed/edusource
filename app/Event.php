<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Page;

class Event extends Model
{
    protected $fillable = [
        'title', 'seo_title', 'description', 'seo_description', 'seo_keywords',
        'tags', 'type', 'context', 'page', 'place','place_cord',
        'datetime'
    ];

    public function getPage(){
        return $this->hasOne('App\Page','id','page');
    }
}

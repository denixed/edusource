<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('admin.penal.ga')->name('admin.')->middleware('verified','admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::prefix('organization')->name('organization.')->group(function () {
        Route::get('/', 'Admin\OrganizationController@index')->name('index');
        Route::get('/list', 'Admin\OrganizationController@index')->name('list');
        Route::get('/create', 'Admin\OrganizationController@create')->name('create');
        Route::post('/create', 'Admin\OrganizationController@store')->name('store');
        Route::get('/{organization}', 'Admin\OrganizationController@read')->name('read');
        Route::get('/{organization}/edit', 'Admin\OrganizationController@edit')->name('edit');
        Route::post('/{organization}/edit', 'Admin\OrganizationController@update')->name('update');
        Route::get('/{organization}/delete', 'Admin\OrganizationController@destroy')->name('delete');
        Route::post('/create', 'Admin\OrganizationController@store')->name('store');
        Route::prefix('vuz')->name('vuz.')->group(function(){
            Route::get('/{organization}/editProhodBalls', 'Admin\OrganizationController@vuzEditProhodBalls')->name('editProhodBalls');
            Route::post('/{organization}/updateProhodBalls', 'Admin\OrganizationController@vuzUpdateProhodBalls')->name('updateProhodBalls');
        });

    });
    /*Route::prefix('vuzFaculty')->name('vuz_faculty.')->group(function () {
        Route::get('/vuz/{vuz}', 'Admin\VuzFacultyController@index')->name('index');
        Route::get('/vuz/{vuz}/list', 'Admin\VuzFacultyController@index')->name('list');
        Route::get('/vuz/{vuz}/create', 'Admin\VuzFacultyController@create')->name('create');
        Route::post('/vuz/{vuz}/create', 'Admin\VuzFacultyController@store')->name('store');
        Route::get('/{vuzFaculty}', 'Admin\VuzFacultyController@read')->name('read');
        Route::get('/{vuzFaculty}/edit', 'Admin\VuzFacultyController@edit')->name('edit');
        Route::post('/{vuzFaculty}/edit', 'Admin\VuzFacultyController@update')->name('update');
        Route::get('/{vuzFaculty}/delete', 'Admin\VuzFacultyController@destroy')->name('delete');
        Route::post('/vuz/{vuz}/create', 'Admin\VuzFacultyController@store')->name('store');
    });*/
    Route::prefix('vuzDivision')->name('vuz_division.')->group(function () {
        Route::get('/vuz/{vuz}', 'Admin\VuzDivisionController@index')->name('index');
        Route::get('/vuz/{vuz}/list', 'Admin\VuzDivisionController@index')->name('list');
        Route::get('/vuz/{vuz}/create', 'Admin\VuzDivisionController@create')->name('create');
        Route::post('/vuz/{vuz}/create', 'Admin\VuzDivisionController@store')->name('store');
        Route::get('/{vuzDivision}', 'Admin\VuzDivisionController@read')->name('read');
        Route::get('/{vuzDivision}/edit', 'Admin\VuzDivisionController@edit')->name('edit');
        Route::post('/{vuzDivision}/edit', 'Admin\VuzDivisionController@update')->name('update');
        Route::get('/{vuzDivision}/delete', 'Admin\VuzDivisionController@destroy')->name('delete');
        Route::post('/vuz/{vuz}/create', 'Admin\VuzDivisionController@store')->name('store');
    });
    Route::prefix('speciality')->name('speciality.')->group(function () {
        Route::get('/', 'AdminController@specialityIndex')->name('index');
        Route::get('/list', 'AdminController@specialityIndex')->name('list');
        Route::get('/create', 'AdminController@specialityCreate')->name('create');
        Route::post('/create', 'AdminController@specialityStore')->name('store');
        Route::get('/{speciality}', 'AdminController@specialityRead')->name('read');
        Route::get('/{speciality}/edit', 'AdminController@specialityEdit')->name('edit');
        Route::post('/{speciality}/edit', 'AdminController@specialityUpdate')->name('update');
        Route::get('/{speciality}/delete', 'AdminController@specialityDestroy')->name('delete');
        Route::post('/create', 'AdminController@specialityStore')->name('store');
    });
    Route::prefix('eduForm')->name('eduForm.')->group(function () {
        Route::get('/', 'AdminController@eduFormIndex')->name('index');
        Route::get('/list', 'AdminController@eduFormIndex')->name('list');
        Route::get('/create', 'AdminController@eduFormCreate')->name('create');
        Route::post('/create', 'AdminController@eduFormStore')->name('store');
        Route::get('/{eduForm}', 'AdminController@eduFormRead')->name('read');
        Route::get('/{eduForm}/edit', 'AdminController@eduFormEdit')->name('edit');
        Route::post('/{eduForm}/edit', 'AdminController@eduFormUpdate')->name('update');
        Route::get('/{eduForm}/delete', 'AdminController@eduFormDestroy')->name('delete');
    });

    Route::prefix('profile')->name('profile.')->group(function () {
        Route::get('/', 'AdminController@profileIndex')->name('index');
        Route::post('/', 'AdminController@profileUpdate')->name('update');
    });
    Route::prefix('post')->name('post.')->group(function () {
        Route::get('/{post}', 'Admin\PostController@read')->name('read');
        Route::get('/{post}/edit', 'Admin\PostController@edit')->name('edit');
        Route::post('/{post}/edit', 'Admin\PostController@update')->name('update');
        Route::get('/post}/delete', 'Admin\PostController@destroy')->name('delete');
        Route::get('/{parent_type}/{parent_id}', 'Admin\PostController@index')->name('index');
        Route::get('/{parent_type}/{parent_id}/list', 'Admin\PostController@index')->name('list');
        Route::get('/{parent_type}/{parent_id}/create', 'Admin\PostController@create')->name('create');
        Route::post('/{parent_type}/{parent_id}/create', 'Admin\PostController@store')->name('store');
    });
    Route::prefix('exam')->name('exam.')->group(function () {
        Route::get('/', 'AdminController@examIndex')->name('index');
        Route::get('/list', 'AdminController@examIndex')->name('list');
        Route::get('/create', 'AdminController@examCreate')->name('create');
        Route::post('/create', 'AdminController@examStore')->name('store');
        Route::get('/{exam}', 'AdminController@examRead')->name('read');
        Route::get('/{exam}/edit', 'AdminController@examEdit')->name('edit');
        Route::post('/{exam}/edit', 'AdminController@examUpdate')->name('update');
        Route::get('/{exam}/delete', 'AdminController@examDestroy')->name('delete');
    });
    Route::prefix('subject')->name('subject.')->group(function () {
        Route::get('/e{exam}/', 'AdminController@subjectIndex')->name('index');
        Route::get('/e{exam}/list', 'AdminController@subjectIndex')->name('list');
        Route::get('/e{exam}/create', 'AdminController@subjectCreate')->name('create');
        Route::post('/e{exam}/create', 'AdminController@subjectStore')->name('store');
        Route::get('/{subject}', 'AdminController@subjectRead')->name('read');
        Route::get('/{subject}/edit', 'AdminController@subjectEdit')->name('edit');
        Route::post('/{subject}/edit', 'AdminController@subjectUpdate')->name('update');
        Route::get('/{subject}/delete', 'AdminController@subjectDestroy')->name('delete');
    });
    Route::prefix('theme')->name('theme.')->group(function () {
        Route::get('/s{subject}/', 'AdminController@themeIndex')->name('index');
        Route::get('/s{subject}/list', 'AdminController@themeIndex')->name('list');
        Route::get('/s{subject}/create', 'AdminController@themeCreate')->name('create');
        Route::post('/s{subject}/create', 'AdminController@themeStore')->name('store');
        Route::get('/{theme}', 'AdminController@themeRead')->name('read');
        Route::get('/{theme}/edit', 'AdminController@themeEdit')->name('edit');
        Route::post('/{theme}/edit', 'AdminController@themeUpdate')->name('update');
        Route::get('/{theme}/delete', 'AdminController@themeDestroy')->name('delete');
    });
    Route::prefix('prot')->name('prot.')->group(function () {
        Route::get('/th{theme}/create', 'AdminController@protCreate')->name('create');
        Route::post('/th{theme}/create', 'AdminController@protStore')->name('store');
        Route::get('/{prot}', 'AdminController@protRead')->name('read');
        Route::get('/{prot}/edit', 'AdminController@protEdit')->name('edit');
        Route::post('/{prot}/edit', 'AdminController@protUpdate')->name('update');
        Route::get('/{prot}/delete', 'AdminController@protDestroy')->name('delete');
    });
    Route::prefix('page')->name('page.')->group(function () {
        Route::get('/create', 'AdminController@pageCreate')->name('create');
        Route::post('/create', 'AdminController@pageStore')->name('store');
        Route::get('/list', 'AdminController@pageIndex')->name('list');
        Route::get('/{page}', 'AdminController@pageRead')->name('read');
        Route::get('/{page}/edit', 'AdminController@pageEdit')->name('edit');
        Route::post('/{page}/edit', 'AdminController@pageUpdate')->name('update');
        Route::get('/{page}/delete', 'AdminController@pageDestroy')->name('delete');
        Route::get('/', 'AdminController@pageIndex')->name('index');
    });
    Route::prefix('media-size')->name('mediaSize.')->group(function () {
        Route::get('/', 'Admin\MediaSizeController@index')->name('index');
        Route::get('/list', 'Admin\MediaSizeController@index')->name('list');
        Route::get('/create', 'Admin\MediaSizeController@create')->name('create');
        Route::post('/create', 'Admin\MediaSizeController@store')->name('store');
        Route::get('/{mediaSize}', 'Admin\MediaSizeController@read')->name('read');
        Route::get('/{mediaSize}/edit', 'Admin\MediaSizeController@edit')->name('edit');
        Route::post('/{mediaSize}/edit', 'Admin\MediaSizeController@update')->name('update');
        Route::get('/{mediaSize}/delete', 'Admin\MediaSizeController@destroy')->name('delete');
        Route::post('/create', 'Admin\MediaSizeController@store')->name('store');

    });
    Route::prefix('media')->name('media.')->group(function () {
        Route::get('/', 'Admin\MediaController@index')->name('index');
        Route::get('/list', 'Admin\MediaController@list')->name('list');
        Route::post('/{mediaSize}/edit', 'Admin\MediaController@update')->name('update');
        Route::get('/{mediaSize}/delete', 'Admin\MediaController@destroy')->name('delete');
        Route::post('/upload', 'Admin\MediaController@store')->name('store');
    });

    Route::prefix('lessons-block')->name('lessons.block.')->group(function () {
        Route::get('/', 'Admin\LessonsBlockController@index')->name('index');
        Route::get('/list', 'Admin\LessonsBlockController@index')->name('list');
        Route::get('/create', 'Admin\LessonsBlockController@create')->name('create');
        Route::post('/create', 'Admin\LessonsBlockController@store')->name('store');
        Route::get('/{lessonsBlock}', 'Admin\LessonsBlockController@show')->name('read');
        Route::get('/{lessonsBlock}/edit', 'Admin\LessonsBlockController@edit')->name('edit');
        Route::post('/{lessonsBlock}/edit', 'Admin\LessonsBlockController@update')->name('update');
        Route::get('/{lessonsBlock}/delete', 'Admin\LessonsBlockController@destroy')->name('delete');
        Route::post('/create', 'Admin\LessonsBlockController@store')->name('store');
    });
    Route::prefix('lesson')->name('lesson.')->group(function () {
        Route::get('/block{lessonsBlock}/', 'Admin\LessonController@index')->name('index');
        Route::get('/block{lessonsBlock}/list', 'Admin\LessonController@index')->name('list');
        Route::get('/block{lessonsBlock}/create', 'Admin\LessonController@create')->name('create');
        Route::post('/block{{lessonsBlock}}/create', 'Admin\LessonController@store')->name('store');
        Route::get('/l{lesson}', 'Admin\LessonController@show')->name('read');
        Route::get('/l{lesson}/edit', 'Admin\LessonController@edit')->name('edit');
        Route::post('/l{lesson}/edit', 'Admin\LessonController@update')->name('update');
        Route::get('/l{lesson}/delete', 'Admin\LessonController@destroy')->name('delete');
        Route::post('/block{lessonsBlock}/create', 'Admin\LessonController@store')->name('store');
    });
    


});
Route::get('/postAuthRedirect', function(){
    if(!Auth::user()) return redirect(route('login'));
    if(Auth::user()->isAdmin() || Auth::user()->isAuthor() ) return redirect(route('admin.index'));
    return redirect(route('home'));
})->name('postAuthRedirect');

Route::domain('vuzy.penal.ga')->name('vuz.')->group(function(){
    Route::get('/', 'VuzController@index')->name('index');
    Route::get('/about/{vuz}', 'VuzController@show')->name('read');
    Route::get('/about/{vuz}/{tab}', 'VuzController@showTab')->name('showTab');
    Route::get('/faculty/{faculty}', 'VuzController@showFaculty')->name('faculty.read');
    Route::get('/division/{division}', 'VuzController@showDivision')->name('division.read');
});

Route::domain('kursy.penal.ga')->name('kurs.')->group(function(){
    Route::get('/', 'OrganizationController@indexKurs')->name('index');
});
Route::domain('olymps.penal.ga')->name('page.')->group(function(){
    Route::get('/', 'PageController@index')->name('index');
    Route::get('/page/{page}', 'PageController@show')->name('read');
    Route::get('/page/{page}/{tab}', 'PageController@showTab')->name('showTab');
});

Route::domain('learn.penal.ga')->name('lesson.')->group(function(){

        Route::get('/', 'LessonController@index')->name('index');
        Route::get('/kurs/{lessonsBlock}', 'LessonController@block')->name('block');
        Route::get('/lesson/{lesson}', 'LessonController@read')->name('read');

});
Route::domain('penal.ga')->group(function(){


    Route::name('organization.')->group(function () {
        Route::post('/review/{organization}/create', 'ReviewController@store')->name('review.store');
        Route::get('/organization/', 'OrganizationController@index')->name('index');
        Route::get('/organization/{organization}', 'OrganizationController@show')->name('read');
        Route::get('/organization/{organization}/{tab}', 'OrganizationController@showTab')->name('showTab');
    });
    Route::name('post.')->group(function () {
        Route::get('/posts', 'PostController@index')->name('index');
        Route::get('/post/{post}', 'PostController@show')->name('read');
        Route::get('/p/{post_url}', 'PostController@showByLink')->name('readByLink');
    });

    
    Route::name('testing.')->group(function () {
        Route::get('/testing', 'TestingController@index')->name('index');
        Route::get('/test/subject{subject}', 'TestingController@subject')->name('subject');
    });


    Route::get('/','FeedController@index')->name('feed');
    Route::get('/news','FeedController@news')->name('feed.news');
    Route::get('/articles','FeedController@articles')->name('feed.articles');

    Route::get('/home', 'HomeController@index')->name('home');

});

use Illuminate\Support\Facades\Route;


Route::get('/routes', function() {

    $routeCollection = Route::getRoutes();

    echo "<table style='width:100%'>";
    echo "<tr>";
    echo "<td width='10%'><h4>HTTP Method</h4></td>";
    echo "<td width='10%'><h4>Route</h4></td>";
    echo "<td width='80%'><h4>Corresponding Action</h4></td>";
    echo "</tr>";
    //dd($routeCollection);
    foreach ($routeCollection as $i=>$value) {

        echo "<tr>";
        //echo "<td>" . isset($value->action['domain']) . "</td>";
        //echo "<td>" . $value->action['as']||'' . "</td>";
        echo "<td>" . implode(', ',$value->methods) . "</td>";
        echo "<td>" . str_replace('web, ','',implode(', ',$value->action['middleware'])) . "</td>";
        echo "<td>" . $value->uri . "</td>";
        echo "<td>" . str_replace('App\Http\Controllers\\','',$value->getActionName()) . "</td>";
        echo "<td>" . $value->getName() . "</td>";
        echo "<td>" . $value->domain() . "</td>";
        echo "</tr>";
    }
    echo "</table>";
});


Auth::routes(['verify' => true]);

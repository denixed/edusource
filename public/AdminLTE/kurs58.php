<?php
/*
Template Name: Страница каждого курса 5-8 класс
*/
?>
<?php get_header(); ?>
  <?php include(TEMPLATEPATH . '/inscription_58.php'); ?>
   
        <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
        <div class="clear"></div>
        <!-- Info -->
        <div id="info">
          <h1 style="margin-top: -5px;"><?php echo get_the_excerpt(); ?></h1>
          <?php include(TEMPLATEPATH . '/kurs_subheader.php'); ?>
          <div id="video">
            <?php echo get_the_post_thumbnail(null,'post-thumbnail',array('alt' => get_the_excerpt())); ?>
          </div>
          <?php include(TEMPLATEPATH . '/action.php'); ?>        </div>
      </div>
    </div>
    <div class="clear"></div>
    <!-- Kurs-info -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div id="kurs-info">
    <div class="content">
    <?php the_content(); ?>
    </div>
    </div>
<div class="rek">
<div class="content">
<h2>С ЭТИМ КУРСОМ НАШИ УЧЕНИКИ ПОСЕЩАЮТ</h2>
<?php echo do_shortcode("[pagelist child_of=parent exclude=current]"); ?>
</div>
</div>
    <?php endwhile; ?>
    <!-- post navigation -->
    <?php else: ?>
    Ничего не найдено...
    <?php endif; ?>
    <div class="clear"></div>
    <!-- Why -->
    <div id="why">
      <div class="content">
        
<?php 
query_posts('p=176');
if (have_posts()) :
while (have_posts()) : the_post();
print get_the_content();
endwhile;
endif;
?>
      </div>
    </div>
    <div class="clear"></div>

<!-- Price -->
    <div id="price">
      <div>
          <br><br><h2 style="color:#1a1a1a;">Стоимость обучения</h2><br><p style="text-align:center;font-size:200%;color:#34968c;"><strong>3 ФОРМАТА ОБУЧЕНИЯ НА ЛЮБОЙ БЮДЖЕТ</strong></p><br><br>
<?php 
query_posts('p=173');
if (have_posts()) :
while (have_posts()) : the_post();
print get_the_content();
endwhile;
endif;
?>
      </div>
    </div>
    <div class="clear"></div>

<!-- Employment Benefits -->
    <div id="employment_benefits">
        <div class="content">
            <?php 
query_posts('p=1922');
if (have_posts()) :
while (have_posts()) : the_post();
print get_the_content();
endwhile;
endif;
?>
        </div>

    </div>
<div class="clear"></div>

<!-- AboutVideo -->
<?php include ('aboutvideo-block.php');?>

<!-- Teachers -->

<?php include ('ourtutors-block.php');?>

<!-- Reviews -->
<?php include ('reviews-block.php');?>


<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=OJ09kQC8shZje*XDWk7EuD5x1WgPt3r2wPfXEMaW8Y0l1Pg3HoOZK4F5OHreS5WaxElg8Gcxb63djBp0BGQwsQ3stJHos73s1V*pQwIuSE2ZTnmmB1URuOQHdVz4d7*gutFmaA7o*zjvKLBTq1JKQEfHsb*IyvuM28Wsvlfrhg8-';</script>

    <?php get_footer(); ?>
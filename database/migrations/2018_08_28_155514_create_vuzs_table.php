<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVuzsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vuzs', function (Blueprint $table) {
            $table->integer('id')->index()->unique();
            $table->string('city')->nullable();
            $table->integer('min_ball')->nullable();
            $table->integer('budget')->nullable();
            $table->string('site')->nullable();
            $table->boolean('hostel')->nullable();
            $table->boolean('military')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vuzs');
    }
}

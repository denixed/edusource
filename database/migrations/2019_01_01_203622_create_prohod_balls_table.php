<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProhodBallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prohod_balls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vuz_id')->index();
            $table->integer('speciality_id')->index();
            $table->integer('year');
            $table->integer('ball');
            $table->string('subjects');
            $table->integer('division_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prohod_balls');
    }
}

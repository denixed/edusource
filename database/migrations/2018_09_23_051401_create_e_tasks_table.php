<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateETasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('e_prot_id',false,true);
            $table->boolean('level');
            $table->text('content');
            $table->text('solution');
            $table->string('answer');
            $table->string('choice');
            $table->integer('e_common_content_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_tasks');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->json('authors');
            $table->text('description');
            $table->string('image_cover');
            $table->string('image_preview');
            $table->string('subject');
            $table->string('exam');
            $table->unsignedBigInteger('views');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_blocks');
    }
}

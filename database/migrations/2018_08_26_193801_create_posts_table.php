<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(0);
            $table->string('title');
            $table->string('link')->unique();
            $table->string('description');
            $table->string('seo_keywords');
            $table->string('tags');
            $table->string('type');
            $table->longText('context');
            $table->string('preview_photo')->nullable();
            $table->boolean('show_in_feed')->default(0);
            $table->integer('organization_id')->nullable();
            $table->integer('page_id')->nullable();
            $table->integer('event_id')->nullable();
            $table->integer('views')->default(0);
            $table->timestamps();
        });
        DB::statement('ALTER TABLE posts ADD FULLTEXT fulltext_index (title, description, seo_keywords, tags, context )');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('contact_full_name');
            $table->string('contact_number');
            $table->string('logo_image');
            $table->string('type');
            $table->longText('about')->nullable();
            $table->string('address')->nullable();
            $table->string('cord')->nullable();
            $table->json('additional');
            $table->integer('status');
            $table->integer('views')->default(0);
            $table->timestamps();
        });
        DB::statement('ALTER TABLE organizations ADD FULLTEXT fulltext_index (name, description, about)');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
